# czech_centres_web

# Zadání
Navrhněte interaktivní zážitek (e-learningový materiál) seznamující laického uživatele s vývojem her prostřednictvím
průchodu jednotlivými tématy/fázemi herního vývoje ( jako např. nápad, koncept, tvorba grafiky, zvuku, marketing, práce
s hráčem, ... ). Kde to bude možné propojte jednotlivá témata s českou scénou herního vývoje a uveďte příklady.
Zakomponujte vhodné herní prvky pro udržení uživatelovy pozornosti.
Implementujte zážitek tak, aby se dal umístit do webové stránky. Zpracujte alespoň 9 témat/fází, jejich výběr konzultujte
s vedoucím. Navrhněte a realizujte způsob monitorování průchodu uživatelů zážitkem. Finální implementaci otestujte s
minimálně šesti uživateli.

# Releases
## 0.1.X
### Release 0.1.8.3
Přidání odkazu na tento repositář.
### Release 0.1.8.1
Oprava chyb.
### Release 0.1.8
Závěrečné opravy. Verze před odevzdáním.
### Release 0.1.7
Závěrečné úpravy.
### Release 0.1.6
Přidání překážek a závěrečných úprav. Hra je nyní kompletní a plně funkční.
### Release 0.1.5
Diplomy a konec je funkční. Hra je nyní hratelná od začátku do konce. Chybí pouze extra překážky.
### Release 0.1.4
Přidání Google Analytics tagu.
#### Release 0.1.4.fix
Presun Tagu Google Analytics do hlavicky.
### Release 0.1.3
Fix World do testování.
### Release 0.1.2
Přidání diplomů. Fáze do testování.
### Release 0.1.1
Fixes.
### Release 0.1.0
Pridani zvuku, titulni strany a intro mistnosti.
## Release 0.0.X
### Release 0.0.9
Dodelani mistnosti. Vsechny jsou nyni hotove a je potreba dodelat jen vylepseni a medaile.
### Release 0.0.8
Dodelani místností až do 04. Cela hra, chybi pouze obsah a male bugy.
### Release 0.0.7
Velke vylepseni cele hry. Vytvorena sprava jednotlivych mistnosti. Pridelane postavy, jejich chovani a k tomu vety a prace s jazyky.
### Release 0.0.6
Organizace souborů. Lepší správa místností.
### Release 0.0.5
Funkční přesun mezi místnostmi a plné kolize.

