const tile_size = 16; // Size of a tile in pixels

let language = "English"; // Default language

const utils = {
  defaultFrameRate: 1 / 60, // Default frame rate for animations

  /**
   * Convert tile grid units to pixel units.
   * @param {number} grid - The grid units.
   * @returns {number} - The pixel units.
   */
  tileToPixel(grid) {
    return grid * tile_size;
  },

  /**
   * Convert grid coordinates to pixel position.
   * @param {object} pos - The grid coordinates.
   * @returns {object} - The pixel position.
   */
  gridToPos(pos) {
    return { x: pos.x * tile_size, y: pos.y * tile_size };
  },

  /**
   * Convert pixel position to grid coordinates.
   * @param {object} pos - The pixel position.
   * @returns {object} - The grid coordinates.
   */
  posToGrid(pos) {
    const x = Math.floor(pos.x / tile_size);
    const y = Math.floor(pos.y / tile_size);
    return { x, y };
  },

  map: {
    center: {
      x: 14 * tile_size,
      y: 7 * tile_size,
    },
  },

  /**
   * Get grid coordinates as a string.
   * @param {number} x - The x coordinate.
   * @param {number} y - The y coordinate.
   * @returns {string} - The coordinates as a string.
   */
  asGridCoords(x, y) {
    return `${x * tile_size},${y * tile_size}`;
  },

  /**
   * Get the next position based on the initial position and direction.
   * @param {object} initialPosition - The initial position.
   * @param {string} direction - The direction to move.
   * @returns {object} - The next position.
   */
  nextPosition(initialPosition, direction) {
    let x = initialPosition.x;
    let y = initialPosition.y;
    const size = tile_size;
    if (direction === "up") {
      y -= size;
    } else if (direction === "down") {
      y += size;
    } else if (direction === "left") {
      x -= size;
    } else if (direction === "right") {
      x += size;
    }
    return { x, y };
  },

  /**
   * Set the current language for translations.
   * @param {string} lang - The language to set.
   */
  setLanguage(lang) {
    gtag("event", "language_change", {
      lang_before: utils.language,
      lang_after: lang,
    });
    utils.language = lang;
  },

  /**
   * Get the translated text for a given key.
   * @param {string} phraseKey - The key of the phrase to translate.
   * @returns {string} - The translated phrase.
   */
  getTranslation(phraseKey) {
    console.log("Language:", utils.language);
    if (translations.hasOwnProperty(phraseKey)) {
      if (
        translations[phraseKey].hasOwnProperty(utils.language) &&
        translations[phraseKey][utils.language] !== ""
      ) {
        return translations[phraseKey][utils.language];
      }
    }
    return phraseKey;
  },

  halls: {
    doors: {
      "13,3": "room_1_gd",
      "25,3": "room_2_vis",
      "13,15": "room_3_gd",
      "25,15": "room_4_gd",
    },
  },

  character: {
    size: {
      width: 12,
      height: 14,
    },
    offset: {
      x: 9,
      y: 12,
    },
    speed: 1,
  },

  /**
   * Emit a custom event.
   * @param {string} name - The name of the event.
   * @param {object} detail - The detail of the event.
   */
  emitEvent(name, detail) {
    document.dispatchEvent(new CustomEvent(name, { detail }));
  },

  actionsKeys: ["Enter", "Space", "KeyF"],

  /**
   * Get the opposite direction.
   * @param {string} direction - The current direction.
   * @returns {string} - The opposite direction.
   */
  oppositeDirection(direction) {
    switch (direction) {
      case "up":
        return "down";
      case "down":
        return "up";
      case "left":
        return "right";
      case "right":
        return "left";
    }
  },

  /**
   * Wait for a specified amount of milliseconds.
   * @param {number} ms - The number of milliseconds to wait.
   * @returns {Promise} - A promise that resolves after the specified time.
   */
  wait(ms) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, ms);
    });
  },

  /**
   * Get a random talk message.
   * @returns {object} - An object with type and text properties.
   */
  getRandomTalk() {
    const talks = [
      "Leave me alone!",
      "This game is not for me.",
      "It's too hard!",
      "I'm not a gamer.",
      "It's boring!",
      "It's too easy!",
    ];

    const toBeReturn = {
      type: "textMessage",
      text: talks[Math.floor(Math.random() * talks.length)],
    };

    return toBeReturn;
  },
};
