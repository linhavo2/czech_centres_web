/**
 * Input class to handle player input for movement and actions.
 * Maps key presses to directions and tracks held keys.
 */
class Input {
  constructor() {
    this.heldDirections = []; // Array to track currently held directions

    // Mapping of key codes to directions
    this.map = {
      ArrowUp: "up",
      ArrowDown: "down",
      ArrowLeft: "left",
      ArrowRight: "right",
      KeyW: "up",
      KeyS: "down",
      KeyA: "left",
      KeyD: "right",
    };

    this.shift = false; // Flag to track if the Shift key is held
  }

  /**
   * Get the current direction based on the held keys.
   * @returns {string} - The current direction.
   */
  get direction() {
    return this.heldDirections[0]; // Return the first held direction
  }

  /**
   * Get the status of the Shift key.
   * @returns {boolean} - True if the Shift key is held, otherwise false.
   */
  get shiftPressed() {
    return this.shift;
  }

  /**
   * Initialize event listeners for keydown and keyup events.
   */
  init() {
    document.addEventListener("keydown", (e) => {
      const dir = this.map[e.code];

      // Add the direction to heldDirections if it's not already present
      if (dir && this.heldDirections.indexOf(dir) === -1) {
        this.heldDirections.unshift(dir);
      }

      // Set the Shift key flag if either Shift key is pressed
      if (e.code === "ShiftLeft" || e.code === "ShiftRight") {
        this.shift = true;
      }
    });

    document.addEventListener("keyup", (e) => {
      const dir = this.map[e.code];
      const index = this.heldDirections.indexOf(dir);

      // Remove the direction from heldDirections if it's present
      if (index > -1) {
        this.heldDirections.splice(index, 1);
      }

      // Unset the Shift key flag if either Shift key is released
      if (e.code === "ShiftLeft" || e.code === "ShiftRight") {
        this.shift = false;
      }
    });
  }
}
