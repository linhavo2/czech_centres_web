const translations = {
    "Hello player!": {
        "Czech": "Ahoj hráči!"
    },
    "What do you want?!": {
        "Czech": "Co chceš?!"
    },
    "What are you looking for? Happiness? Haha": {
        "Czech": "Co tam hledáš? Štěstí? Haha"
    },
    "I'm busy!": {
        "Czech": "Jsem zaneprázdněn!"
    },
    "Change the language": {
        "Czech": "Změnit jazyk"
    },
    "Start game": {
        "Czech": "Začít hru"
    },
    "Continue": {
        "Czech": "Pokračovat"
    },
    "Language": {
        "Czech": "Jazyk"
    },
    "Close": {
        "Czech": "Zavřít"
    },
    "Return to the game": {
        "Czech": "Vrátit do hry"
    },
    "Next": {
        "Czech": "Další"
    },
    "Pause Menu": {
        "Czech": "Pauza"
    },
    "": {
        "Czech": ""
    },
    "# Start:": {
        "Czech": "# Start:"
    },
    "The journey begins with an Idea.": {
        "Czech": "Cesta začíná nápadem."
    },
    "It's like the first spark of a fire, where developers brainstorm different concepts, imagining the kind of game they want to create.": {
        "Czech": "Je to jako první jiskra ohně, kdy vývojáři vymýšlejí různé koncepty a představují si, jakou hru chtějí vytvořit."
    },
    "This initial burst of creativity sets the course for everything that follows, igniting the enthusiasm to bring this game to life.": {
        "Czech": "Tento počáteční výbuch kreativity udává směr všemu, co bude následovat, a zažehne nadšení přivést tuto hru k životu."
    },
    "They then set their Goals, deciding what they aim to achieve with the game.": {
        "Czech": "Poté si stanoví Cíle a rozhodnou se, čeho chtějí hrou dosáhnout."
    },
    "Whether it's to create an exciting adventure or a compelling story, these goals keep the team focused and driven.": {
        "Czech": "Ať už jde o vytvoření vzrušujícího dobrodružství nebo poutavého příběhu, tyto cíle udržují tým soustředěný a cílevědomý."
    },
    "It's like setting personal milestones to ensure they reach their destination.": {
        "Czech": "Je to jako stanovení osobních milníků, které jim zajistí dosažení cíle."
    },
    "Next comes Planning. This is where the team organizes the road ahead, deciding who will work on what and when.": {
        "Czech": "Dále následuje plánování. Zde tým organizuje cestu vpřed a rozhoduje, kdo, na čem a kdy bude pracovat."
    },
    "It's like planning a road trip; without a clear plan, they could get lost along the way, leading to chaos and delays.": {
        "Czech": "Je to jako plánování cesty; bez jasného plánu by se mohli na cestě ztratit, což by vedlo k chaosu a zpoždění."
    },
    "With a plan in hand, the developers move to the Concept phase.": {
        "Czech": "S plánem v ruce se vývojáři přesunou do fáze Koncepce."
    },
    "This is like sketching the blueprint of a building, outlining the game's look, feel, and basic structure.": {
        "Czech": "To je jako načrtnout plán budovy, nastínit vzhled, atmosféru a základní strukturu hry."
    },
    "Here, they create a shared vision that guides everyone involved, ensuring they stay on track.": {
        "Czech": "Zde vytvářejí společnou vizi, kterou se řídí všichni zúčastnění a která zajišťuje, že zůstanou na správné cestě."
    },
    "# Game Design:": {
        "Czech": "# Návrh hry:"
    },
    "Now the team dives into Game Design, starting with Game Mechanics. These are the rules and systems that shape how the game is played.": {
        "Czech": "Nyní se tým ponoří do herního designu, počínaje herními mechanismy. Jedná se o pravidla a systémy, které určují, jak se hra hraje."
    },
    "They define what players can do and how they win, ensuring the game is balanced and fun.": {
        "Czech": "Definují, co mohou hráči dělat a jak vyhrávat, a zajišťují, aby hra byla vyvážená a zábavná."
    },
    "The next step is creating Characters. These are the heroes, villains, and sidekicks that players will interact with and control.": {
        "Czech": "Dalším krokem je tvorba postav. To jsou hrdinové, padouši a pomocníci, s nimiž budou hráči komunikovat a které budou ovládat."
    },
    "Like casting for a movie, choosing the right characters brings the game to life and adds depth to the story.": {
        "Czech": "Podobně jako při obsazování do filmu, i výběr správných postav hru oživí a dodá příběhu hloubku."
    },
    "The Map is where the game takes place, and developers map out the world that players will explore.": {
        "Czech": "Mapa je místo, kde se hra odehrává, a vývojáři mapují svět, který budou hráči prozkoumávat."
    },
    "It's like drawing a treasure map, leading players to hidden secrets and epic journeys. Let's look at this level from above.": {
        "Czech": "Je to jako kreslit mapu pokladů, která hráče vede ke skrytým tajemstvím a epickým cestám."
    },
    "But what holds it all together is the Story. This is the narrative thread that guides players through the game, giving them purpose and motivation.": {
        "Czech": "To, co to všechno drží pohromadě, je však Příběh. Je to příběhová nit, která hráče vede hrou a dává jim smysl a motivaci."
    },
    "A captivating story can turn a simple game into an unforgettable experience, keeping players hooked from start to finish.": {
        "Czech": "Poutavý příběh dokáže z jednoduché hry udělat nezapomenutelný zážitek a udržet hráče v napětí od začátku do konce."
    },
    "# Visuals:": {
        "Czech": "# Vizuální stránka:"
    },
    "With the game design outlined, the team shifts focus to Visuals.": {
        "Czech": "Po nastínění herního designu se tým zaměří na vizuální stránku."
    },
    "This journey starts with Concept Art, where artists sketch the initial ideas for characters, landscapes, and objects.": {
        "Czech": "Tato cesta začíná koncepčním uměním, kdy umělci načrtnou prvotní nápady postav, krajiny a objektů."
    },
    "These early drawings help establish the game's visual style, ensuring it looks cohesive and attractive.": {
        "Czech": "Tyto první kresby pomáhají stanovit vizuální styl hry a zajišťují, že hra bude vypadat soudržně a atraktivně."
    },
    "Now, we will finally talk about updating visuals of the game. Starting with you.": {
        "Czech": "Konečně jsme se dostali k aktualizování vizuálů. Začneme tebou!"
    },
    "What do you think? Do you like your new style? Now for me.": {
        "Czech": "Tak co? Líbí se ti tvůj nový styl? Teď já."
    },
    "Did you expect this? Haha that is what I thought. Now the background. It is a bit boring like this, right?": {
        "Czech": "Čekal jsi tohle? Haha, myslel jsem si to. Teď je na řadě pozadí. Takhle je to celkem nudné."
    },
    "Much better! Now the game looks more interesting.": {
        "Czech": "Mnohem lepší! Teď je celá hra mnohem zajímavější."
    },
    "Using Sprites is important to make the game more appealing to the player. Let's move to the next one.": {
        "Czech": "Sprity jsou důležitým bodem, jak udělat hru poutavější. Jdeme dál!"
    },
    "But it's Animation that truly brings the game to life.": {
        "Czech": "Ale teprve animace hru skutečně oživí."
    },
    "By adding movement and action to characters and objects, animation creates a dynamic and immersive experience.": {
        "Czech": "Tím, že animace přidává postavám a objektům pohyb a akci, vytváří dynamický a pohlcující zážitek."
    },
    "It's like breathing life into a static picture, making the game world feel vibrant and alive. How do you feel? Now for me.": {
        "Czech": "Je to jako vdechnout život statickému obrázku, díky čemuž herní svět působí živě a plnohodnotně. Teď jsem na řadě já."
    },
    "To ensure players can navigate easily, the team designs the UI/UX.": {
        "Czech": "Aby se hráči mohli snadno orientovat, navrhuje tým UI/UX, neboli uživatelské rozhraní."
    },
    "This includes the menus, buttons, and other interface elements that guide players through the game.": {
        "Czech": "To zahrnuje nabídky, tlačítka a další prvky rozhraní, které hráče provedou hrou."
    },
    "A well-designed UI/UX ensures players can focus on the fun without getting lost in complex controls.": {
        "Czech": "Dobře navržené UI/UX zajišťuje, že se hráči mohou soustředit na zábavu, aniž by se ztráceli ve složitém ovládání."
    },
    "# Audio:": {
        "Czech": "# Audio:"
    },
    "The audio journey begins with Music, setting the mood and atmosphere for the game.": {
        "Czech": "Audio cesta začíná hudbou, která nastavuje náladu a atmosféru hry."
    },
    "Like a movie soundtrack, the right music can evoke emotions and create a memorable experience.": {
        "Czech": "Podobně jako filmový soundtrack dokáže správná hudba vyvolat emoce a vytvořit nezapomenutelný zážitek."
    },
    "Whether it's an epic battle theme or a calming background tune, music adds depth to the game.": {
        "Czech": "Ať už se jedná o epické bojové téma nebo uklidňující melodii na pozadí, hudba dodává hře hloubku."
    },
    "Sound Effects are the noises that bring the game world to life.": {
        "Czech": "Zvukové efekty jsou zvuky, které oživují herní svět."
    },
    "From the crunch of footsteps to the roar of explosions, these sounds make the environment feel real and engaging.": {
        "Czech": "Díky těmto zvukům, od skřípění kroků po hukot výbuchů, působí prostředí reálně a poutavě."
    },
    "Properly crafted sound effects enhance immersion, making players feel like they're part of the action.": {
        "Czech": "Správně vytvořené zvukové efekty zvyšují vtažení do děje a dávají hráčům pocit, že jsou součástí akce."
    },
    "Voice Acting adds personality and charm to the game's characters. Voice actors give them unique voices, helping players connect with their journey and story.": {
        "Czech": "Hlasový projev dodává postavám ve hře osobitost a šarm. Hlasoví herci jim propůjčují jedinečné hlasy a pomáhají tak hráčům sžít se s jejich cestou a příběhem."
    },
    "It's like having a great narrator in a book, guiding you through the adventure.": {
        "Czech": "Je to jako mít v knize skvělého vypravěče, který vás provází dobrodružstvím."
    },
    "Finally, Ambient Sounds create the backdrop for the game, providing subtle cues that make the world feel authentic.": {
        "Czech": "A konečně zvuky prostředí vytvářejí kulisu hry a poskytují jemné náznaky, díky nimž svět působí autenticky."
    },
    "Birds chirping, wind rustling, and distant city noise all contribute to a realistic environment. These sounds add an extra layer of immersion, making the game world feel lived in.": {
        "Czech": "Cvrlikání ptáků, šumění větru a vzdálený hluk města - to vše přispívá k realistickému prostředí. Tyto zvuky přidávají další vrstvu vtažení do hry a dodávají hernímu světu pocit života."
    },
    "# Programming:": {
        "Czech": "# Programování:"
    },
    "The programming journey begins with Engine Selection, where developers choose the technology they'll use to build the game.": {
        "Czech": "Vývojáři si vybírají technologii, kterou použijí k vytvoření hry."
    },
    "This decision is crucial, as it determines the game's capabilities and technical framework.": {
        "Czech": "Toto rozhodnutí je klíčové, protože určuje možnosti hry a její technický rámec."
    },
    "It's like picking the right tools for construction; the wrong choice could limit the game's potential.": {
        "Czech": "Je to jako výběr správných nástrojů pro stavbu; špatná volba může omezit potenciál hry."
    },
    "With the engine selected, they focus on Code Architecture, the structure and organization of the codebase.": {
        "Czech": "Po výběru enginu se zaměří na architekturu kódu, tedy strukturu a organizaci kódové základny."
    },
    "This is the backbone of the game, ensuring the code is clean and manageable.": {
        "Czech": "To je páteř hry, která zajišťuje, že kód je čistý a zvládnutelný."
    },
    "A well-architected codebase makes development smoother and reduces the chances of bugs and crashes.": {
        "Czech": "Dobře uspořádaná kódová základna umožňuje plynulejší vývoj a snižuje pravděpodobnost výskytu chyb a pádů."
    },
    "Feature Implementation is where programmers write the code that makes the game work.": {
        "Czech": "Implementace funkcí je místo, kde programátoři píší kód, díky kterému hra funguje."
    },
    "This involves everything from character movement to complex mechanics, turning ideas into reality.": {
        "Czech": "Zahrnuje vše od pohybu postavy až po složité mechaniky a proměňuje nápady ve skutečnost."
    },
    "Proper implementation ensures the game functions smoothly and delivers a seamless experience to players.": {
        "Czech": "Správná implementace zajišťuje hladké fungování hry a poskytuje hráčům bezproblémový zážitek."
    },
    "To keep the game running efficiently, programmers work on Optimization.": {
        "Czech": "Aby hra fungovala efektivně, pracují programátoři na optimalizaci."
    },
    "This involves fine-tuning the code to reduce lag and improve performance.": {
        "Czech": "Ta zahrnuje vyladění kódu s cílem snížit zpoždění a zlepšit výkon."
    },
    "It's like tuning an engine to get the best performance; without optimization, the game could slow down and frustrate players.": {
        "Czech": "Je to jako ladění motoru pro dosažení nejlepšího výkonu; bez optimalizace by se hra mohla zpomalit a hráče frustrovat."
    },
    "Documentation is the final step, where programmers create guides and manuals that explain how the code works.": {
        "Czech": "Dokumentace je posledním krokem, kdy programátoři vytvářejí průvodce a příručky, které vysvětlují, jak kód funguje."
    },
    "This is crucial for helping other developers understand the project and for maintaining it in the future.": {
        "Czech": "To je klíčové pro pomoc ostatním vývojářům při pochopení projektu a pro jeho údržbu v budoucnu."
    },
    "Good documentation ensures the game's longevity and eases the handover to new team members.": {
        "Czech": "Dobrá dokumentace zajišťuje dlouhou životnost hry a usnadňuje předání novým členům týmu."
    },
    "# Testing:": {
        "Czech": "# Testování:"
    },
    "As the game development journey nears completion, it's time for Testing.": {
        "Czech": "Když se cesta vývoje hry blíží ke konci, přichází čas na testování."
    },
    "The first step is Quality Assurance (QA), where testers play the game to find bugs and issues.": {
        "Czech": "Prvním krokem je zajištění kvality (QA), kdy testeři hrají hru, aby našli chyby a problémy."
    },
    "They're like detectives, uncovering hidden problems and ensuring the game is polished before release.": {
        "Czech": "Jsou jako detektivové, odhalují skryté problémy a zajišťují, aby hra byla před vydáním vyladěná."
    },
    "User Experience Testing focuses on gathering feedback from players to understand their experience.": {
        "Czech": "Testování uživatelských zkušeností se zaměřuje na získávání zpětné vazby od hráčů, aby pochopili jejich zkušenosti."
    },
    "This helps identify what's working well and what needs improvement.": {
        "Czech": "To pomáhá určit, co funguje dobře a co je třeba zlepšit."
    },
    "It's a key step in making sure the game is enjoyable and intuitive for players.": {
        "Czech": "Je to klíčový krok k tomu, aby hra byla pro hráče příjemná a intuitivní."
    },
    "Performance Testing is about checking how the game runs on different devices and platforms.": {
        "Czech": "Testování výkonu spočívá v ověření, jak hra běží na různých zařízeních a platformách."
    },
    "Testers ensure the game operates smoothly without lag or crashes, regardless of the hardware.": {
        "Czech": "Testeři zajišťují, aby hra fungovala plynule, bez zpoždění a pádů, bez ohledu na hardware."
    },
    "This is crucial for delivering a consistent experience across a wide range of devices.": {
        "Czech": "To je klíčové pro zajištění konzistentního zážitku na široké škále zařízení."
    },
    "Compatibility Testing ensures the game works across different operating systems and environments.": {
        "Czech": "Testování kompatibility zajišťuje, že hra funguje v různých operačních systémech a prostředích."
    },
    "This step is essential to reach the widest audience, ensuring the game is playable on various platforms.": {
        "Czech": "Tento krok je nezbytný pro oslovení co nejširšího publika, protože zajišťuje, že hra bude hratelná na různých platformách."
    },
    "Without compatibility testing, some players might be unable to enjoy the game.": {
        "Czech": "Bez testování kompatibility by se mohlo stát, že si někteří hráči nebudou moci hru užít."
    },
    "To know that everything is working as intended, the game must be walked through on different devices.": {
        "Czech": "Proto je důležité hru projít na různých zařízeních."
    },
    "# Marketing:": {
        "Czech": "# Marketing:"
    },
    "With the game development nearly complete, the focus shifts to Marketing.": {
        "Czech": "Když je vývoj hry téměř dokončen, pozornost se přesouvá na marketing."
    },
    "The first step is Market Research, where marketers explore what players want in a game.": {
        "Czech": "Prvním krokem je průzkum trhu, v němž marketéři zkoumají, co hráči od hry chtějí."
    },
    "By understanding current trends and preferences, they can tailor the marketing strategy to attract more players.": {
        "Czech": "Pochopením současných trendů a preferencí mohou přizpůsobit marketingovou strategii tak, aby přilákala více hráčů."
    },
    "Branding is about creating a unique identity for the game, including logos, color schemes, and visual designs.": {
        "Czech": "Branding spočívá ve vytvoření jedinečné identity hry, včetně loga, barevných schémat a vizuálních návrhů."
    },
    "A strong brand makes the game recognizable and memorable, helping it stand out in a crowded market.": {
        "Czech": "Díky silné značce je hra rozpoznatelná a zapamatovatelná, což jí pomáhá vyniknout na přeplněném trhu."
    },
    "Effective branding can generate buzz and excitement before the game's release.": {
        "Czech": "Účinný branding může vyvolat rozruch a vzrušení ještě před vydáním hry."
    },
    "Promotional Materials are the ads, trailers, and posters used to showcase the game to the public.": {
        "Czech": "Propagační materiály jsou reklamy, upoutávky a plakáty, které slouží k prezentaci hry veřejnosti."
    },
    "These materials serve as teasers, creating anticipation and drawing interest.": {
        "Czech": "Tyto materiály slouží jako upoutávky, vyvolávají očekávání a přitahují zájem."
    },
    "A compelling promotional campaign can boost pre-orders and build a loyal fan base before launch.": {
        "Czech": "Přesvědčivá propagační kampaň může zvýšit počet předobjednávek a vybudovat věrnou základnu fanoušků ještě před uvedením hry na trh."
    },
    "Community Engagement involves interacting with players and building a community around the game.": {
        "Czech": "Zapojení komunity zahrnuje interakci s hráči a budování komunity kolem hry."
    },
    "This can happen online through social media or in person at gaming events.": {
        "Czech": "To se může dít online prostřednictvím sociálních médií nebo osobně na herních akcích."
    },
    "Engaging with the community creates a sense of belonging and loyalty, fostering a vibrant player base that supports the game over time.": {
        "Czech": "Zapojení do komunity vytváří pocit sounáležitosti a loajality a podporuje živou hráčskou základnu, která hru dlouhodobě podporuje."
    },
    "To find a way, in a world full of games and tastes, into hearts of players is not easy. But it is very important.": {
        "Czech": "Najít si cestu, ve světě plným her a chutí, k srdcím není snadné. Je to ale velmi důležité."
    },
    "# Publishing:": {
        "Czech": "# Vydávání:"
    },
    "Finally, the journey leads to Publishing, where the game is released to the world.": {
        "Czech": "Nakonec cesta vede k vydání, kdy je hra vypuštěna do světa."
    },
    "The first step is Platform Selection, deciding whether the game will be available on computers, consoles, or mobile devices.": {
        "Czech": "Prvním krokem je výběr platformy, kdy se rozhoduje, zda bude hra dostupná na počítačích, konzolích nebo mobilních zařízeních."
    },
    "This decision affects the game's distribution and potential audience.": {
        "Czech": "Toto rozhodnutí ovlivňuje distribuci hry a její potenciální publikum."
    },
    "Distribution is the process of sending the game to stores or online platforms for purchase and download.": {
        "Czech": "Distribuce je proces odeslání hry do obchodů nebo online platforem ke koupi a stažení."
    },
    "It's the step that makes the game accessible to players, allowing them to enjoy the final product.": {
        "Czech": "Je to krok, který zpřístupňuje hru hráčům a umožňuje jim užít si výsledný produkt."
    },
    "Proper distribution is key to reaching a wide audience and generating sales.": {
        "Czech": "Správná distribuce je klíčem k oslovení širokého publika a generování prodejů."
    },
    "Release Planning is where developers set the game's launch date and plan related events or promotions.": {
        "Czech": "Při plánování vydání vývojáři stanoví datum uvedení hry na trh a plánují související akce nebo propagační akce."
    },
    "It's like preparing for a big premiere; proper planning ensures a successful and smooth launch.": {
        "Czech": "Je to jako příprava na velkou premiéru; správné plánování zajišťuje úspěšné a hladké uvedení hry na trh."
    },
    "Effective release planning can create excitement and drive sales.": {
        "Czech": "Efektivní plánování vydání může vyvolat nadšení a podpořit prodeje."
    },
    "Now you have your game released, but the work is not done yet.": {
        "Czech": "Teď máš sice hru vydanou, práce ale nekončí."
    },
    "Post-Launch Support is the ongoing work after the game's release.": {
        "Czech": "Podpora po uvedení hry na trh je průběžná práce po jejím vydání."
    },
    "This involves fixing bugs, adding new features, and addressing player feedback.": {
        "Czech": "Zahrnuje opravu chyb, přidávání nových funkcí a řešení zpětné vazby od hráčů."
    },
    "Post-launch support is crucial for keeping players engaged and extending the game's lifespan.": {
        "Czech": "Podpora po vydání je klíčová pro udržení zájmu hráčů a prodloužení životnosti hry."
    },
    "# End (Recap):": {
        "Czech": "# Konec (rekapitulace):"
    },
    "Congratulations! You have completed the game development journey and released your game to the world.": {
        "Czech": "Gratuluji! Dokončil jsi cestu vývoje hry a vypustil ji do světa."
    },
    "As the journey comes to an end, developers enter the End phase, where they reflect on their achievements and plan for the future.": {
        "Czech": "Když se cesta blíží ke konci, vývojáři vstupují do koncové fáze, kde se zamýšlejí nad svými úspěchy a plánují budoucnost."
    },
    "Reflection is the first step, allowing them to look back at the development process and understand what worked and what could be improved.": {
        "Czech": "Reflexe je prvním krokem, který jim umožní ohlédnout se zpět na proces vývoje a pochopit, co se povedlo a co by se dalo zlepšit."
    },
    "This reflection helps them grow as developers and make better games in the future.": {
        "Czech": "Tato reflexe jim pomáhá růst jako vývojářům a v budoucnu vytvářet lepší hry."
    },
    "Player Feedback is about listening to the players, gathering insights into what they enjoyed and what they found lacking.": {
        "Czech": "Zpětná vazba od hráčů spočívá v naslouchání hráčům a ve shromažďování poznatků o tom, co se jim líbilo a co jim naopak chybělo."
    },
    "This feedback is invaluable for understanding the player's perspective and guiding future development.": {
        "Czech": "Tato zpětná vazba je neocenitelná pro pochopení pohledu hráčů a pro nasměrování budoucího vývoje."
    },
    "It helps developers create games that resonate with their audience.": {
        "Czech": "Pomáhá vývojářům vytvářet hry, které mají u publika ohlas."
    },
    " ": {
        "Czech": " "
    },
    "A Post-Mortem is the final analysis, where developers document the entire game development process.": {
        "Czech": "Post-Mortem je závěrečná analýza, kde vývojáři dokumentují celý proces vývoje hry."
    },
    "This step involves writing down lessons learned, successes, and areas for improvement.": {
        "Czech": "Tento krok zahrnuje sepsání získaných zkušeností, úspěchů a oblastí pro zlepšení."
    },
    "A thorough post-mortem helps developers refine their approach and avoid repeating mistakes in future projects.": {
        "Czech": "Důkladná post-mortem pomáhá vývojářům zdokonalit jejich přístup a vyhnout se opakování chyb v budoucích projektech."
    },
    "After all the hard work, it's time for a Celebration.": {
        "Czech": "Po vší té tvrdé práci je čas na oslavu."
    },
    "This is when developers acknowledge their achievements and celebrate the success of their project.": {
        "Czech": "Tehdy vývojáři uznávají své úspěchy a oslavují úspěch svého projektu."
    },
    "It's like a grand finale, a chance to relax and enjoy the fruits of their labor.": {
        "Czech": "Je to takové velké finále, příležitost odpočinout si a vychutnat si plody své práce."
    },
    "Finally, developers consider their Future Plans.": {
        "Czech": "Nakonec vývojáři zváží své plány do budoucna."
    },
    "This is where they brainstorm ideas for the next game, whether it's a sequel or something entirely new.": {
        "Czech": "Zde probíhá příprava nápadů na další hru, ať už se jedná o pokračování, nebo něco zcela nového."
    },
    "Future plans mark the beginning of another exciting journey in game development, filled with new ideas and endless possibilities.": {
        "Czech": "Plány do budoucna znamenají začátek další vzrušující cesty ve vývoji her, plné nových nápadů a nekonečných možností."
    },
    "# Intro": {
        "Czech": ""
    },
    "Ha, you are finally awake. Hello and welcome to the game! I will be your guide.": {
        "Czech": "Ha, konečně jsi vzhůru! Vítej do hry o vývoji… videoher. Já budu tvým průvodcem."
    },
    "What you will experience is a journey through the world of game development.": {
        "Czech": "Co zažiješ, je cesta světem vývoje her. "
    },
    "There are 9 rooms dedicated to fundamental phases of game development.": {
        "Czech": "Nachází se zde 9 místností věnovaných základním fázím vývoje her."
    },
    "As you progress through the rooms, you will learn about the game development process and the game will progress with you.": {
        "Czech": "Jak budeš postupovat místnostmi, naučíš se proces vývoje her. Dávej pozor, hra se bude vyvíjet s tebou."
    },
    "You don't understand? Don't worry, you will see.": {
        "Czech": "Nerozumíš? Neboj, zažiješ to na vlastní obrazovku."
    },
    "# Diplom": {
        "Czech": ""
    },
    "You have successfully completed room about": {
        "Czech": "Super! Úspěšně jsi dokončil místnost věnované"
    },
    "in game development": {
        "Czech": "ve vývoji videoher"
    },
    "start": {
        "Czech": "začátku"
    },
    "game design": {
        "Czech": "herní design"
    },
    "visual": {
        "Czech": "vizuálům"
    },
    "audio": {
        "Czech": "audiu"
    },
    "programming": {
        "Czech": "programování"
    },
    "testing": {
        "Czech": "testování"
    },
    "marketing": {
        "Czech": "marketingu"
    },
    "publishing": {
        "Czech": "vydání"
    },
    "end": {
        "Czech": "konci"
    },
    "<Unknown room>": {
        "Czech": "<Neznámá místnost>"
    },
    "More about": {
        "Czech": "Více o"
    },
    "## Praises": {
        "Czech": ""
    },
    "Your ideas are now nearly as good as in Charles games!": {
        "Czech": "Teď máš nápady skoro tak dobré,<br>jako je mají v Charles games!"
    },
    "Your storytelling is now like they do in Hangar 13!": {
        "Czech": "Tvoje vyprávění je teď srovnatelné s Hangar 13!"
    },
    "Wow! Your visuals now almost surpass Amanita Design!": {
        "Czech": "Wow! Tvoje vizuály jsou teď skoro stejně neuvěřitelné<br>jako je dělají v Amanita Design!"
    },
    "Your work with audio is almost as good as in Beat Games!": {
        "Czech": "Práce s audiem ti už jde tak dobře jako v Beat Games!"
    },
    "You could beat Factorio under 1:21:56 like nothing now!": {
        "Czech": "Teď už bys zvládl dohrát Factorio pod 1:21:56 jak nic!"
    },
    "Keen Software House and you know something <br> about proper testing!": {
        "Czech": "Keen Software House a ty víte jak hry pořádně otestovat!"
    },
    "Everybody will know about your games now, <br> same as about games from Warhorse Studios!": {
        "Czech": "Všichni teď budou vědět o tvých hrách,<br>stejně jako o těch od Warhorse Studios!"
    },
    "You could make an incubator<br>like they have in Bohemia Interactive!": {
        "Czech": "Nyní už si můžeš si založit inkubátor,<br>jako mají v Bohemia Interactive!"
    },
    "You know what to do after release of your game,<br>as they do in SCS Software!": {
        "Czech": "Teď už víš, co dělat po vydání hry,<br>tak jako to ví v SCS Software!"
    },
    "## End": {
        "Czech": ""
    },
    "Credits": {
        "Czech": "Titulky"
    },
    "CONGRATULATIONS!": {
        "Czech": "GRATULUJI!"
    },
    "You now know about game development everything from A to Z. Almost...": {
        "Czech": "Nyní víte o vývoji her vše od A do Z. Skoro..."
    },
    "If you want to know more, visit": {
        "Czech": "Pokud se chceš dozvědět více, navštiv"
    },
    "You have completed all there is.": {
        "Czech": "Dokončil jsi vše co šlo."
    },
    "End": {
        "Czech": "Konec"
    },
    "Back": {
        "Czech": "Zpět"
    },
    "Author": {
        "Czech": "Autor"
    },
    "Base for sprites": {
        "Czech": "Základ sprites"
    },
    "Background": {
        "Czech": "Pozadí"
    },
    "Character": {
        "Czech": "Postava"
    },
    "Audio": {
        "Czech": "Audio"
    },
    "Or if you want to see how this game was made, visit": {
        "Czech": "Zajímá tě jak byla tato hra vytvořena? Navštiv"
    },
    "# Hall": {
        "Czech": ""
    },
    "This room is hall. It is a place where you can find doors to other rooms. And to which you will return after visiting them.": {
        "Czech": "Tohle je hala. Zde najdeš dveře do všech místností věnovaným jednotlivým fázím vývoje."
    },
    "This is also a place where you will see improvements from each room. So, keep an eye on them!": {
        "Czech": "Taky je to místo, do kterého se vždy vrátíš až místností projdeš. Tady uvidíš všechny ty změny nejlépe."
    },
    "You can walk through the doors to enter the rooms. But remember, you can't walk through the walls! Or do you?": {
        "Czech": "Pokračuj dál skrz další dveře. Nezapomeň, že zdmi procházet nemůžeš. Nebo jo?"
    },
    "Alright, we have went through the Game design room. Visual room awaits us. I can't wait to look normal again.": {
        "Czech": "Tak, Game design máme za sebou. Hurá na vizuály. Už se nemůzu dočkat až budu vypadat normálně."
    },
    "Deadlines are ruthless. You have to be faster next time!": {
        "Czech": "Uzávěrky jsou nekompromisní. Musíš být rychlejší!"
    },
    "You can't be late. You have to be faster!": {
        "Czech": "Teď nemůžeš mít zpoždění. Musíš být rychlejší!"
    },
    "Release date is coming. You have to be faster!": {
        "Czech": "Vydání se blíží. Buď rychlejší!"
    },
    "Producers are pushing whole team to their limits to make sure the game is ready for the release date.": {
        "Czech": "Producenti tlačí celý tým i tebe na samou hranici zvladatelnosti, aby zajistili, že hra bude připravena na vydání."
    },
    "Can you meet the deadline?": {
        "Czech": "Dokážeš stihnout uzávěrku?"
    },
    "Leave me alone!": {
        "Czech": "Nech mě bejt!"
    },
    "This game is not for me.": {
        "Czech": "Tahle hra není pro mě!"
    },
    "It's too hard!": {
        "Czech": "Je to moc těžké!"
    },
    "I'm not a gamer.": {
        "Czech": "Nejsem hráč!"
    },
    "It's boring!": {
        "Czech": "Je to nudné!"
    },
    "It's too easy!": {
        "Czech": "Je to moc jednoduché."
    }
};