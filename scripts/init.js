(function () {
  // Create a new World instance with the game container element
  const world = new World({
    element: document.querySelector(".game-container"),
  });

  // Initialize the world
  world.init();
})();
