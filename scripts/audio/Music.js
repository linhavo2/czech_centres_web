/**
 * Music class to handle background music playback.
 * Allows for playing, stopping, and destroying music with volume control.
 */
class Music {
  constructor() {
    this.volume = 0.15; // Default volume level

    // Load the music file
    this.music = new Audio("./assets/audio/music/music.mp3");
  }

  /**
   * Initialize the music settings and start playing in a loop with muted volume.
   */
  init() {
    this.music.volume = 0; // Start with muted volume
    this.music.loop = true; // Enable looping
    this.music.play(); // Start playing the music
  }

  /**
   * Play the music by setting the volume to the predefined level.
   */
  play() {
    this.music.volume = this.volume;
  }

  /**
   * Stop the music by setting the volume to zero.
   */
  stop() {
    this.music.volume = 0;
  }

  /**
   * Destroy the music object, stopping playback and clearing resources.
   */
  destroy() {
    this.stop(); // Ensure the music is stopped
    this.music.src = ""; // Clear the source of the audio element
    this.music = null; // Dereference the audio object
  }
}
