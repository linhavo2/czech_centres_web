/**
 * StepSound class to handle the playback of step sounds for characters.
 * Allows for configuring different sounds for different characters and controlling playback.
 */
class StepSound {
  constructor(config) {
    this.useSound = config.useSound; // Flag to determine if sound should be used
    this.volume = 0.1; // Default volume level

    // Load the default step sound
    this.step = new Audio("./assets/audio/sounds/step1.mp3");
    this.step.volume = this.volume;
  }

  /**
   * Mount the appropriate step sound based on the character type.
   * @param {string} who - The character type (e.g., "player" or "npc").
   */
  mount(who) {
    switch (who) {
      case "player":
        this.step = new Audio("./assets/audio/sounds/step1.mp3");
        break;
      case "npc":
        this.step = new Audio("./assets/audio/sounds/step2.mp3");
        break;
      default:
        this.step = new Audio("./assets/audio/sounds/step1.mp3");
        break;
    }
    this.step.volume = this.volume; // Ensure the volume is set for the new sound
  }

  /**
   * Play the step sound if sound is enabled.
   */
  play() {
    if (this.useSound) {
      this.step.play();
    }
  }
}
