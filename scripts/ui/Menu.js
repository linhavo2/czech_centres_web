/**
 * Menu class to create and manage a menu with options, keyboard navigation, and descriptions.
 */
class Menu {
  constructor(config = {}) {
    this.options = []; // Set by the setOptions method
    this.up = null; // KeyPressListener for "ArrowUp"
    this.down = null; // KeyPressListener for "ArrowDown"
    this.prevFocus = null; // Previously focused button
    this.descriptionContainer = config.descriptionContainer || null; // Container for descriptions
  }

  /**
   * Set the options for the menu and create the HTML elements for each option.
   * @param {Array} options - Array of option objects with label, handler, etc.
   */
  setOptions(options) {
    this.options = options;
    this.element.innerHTML = this.options
      .map((option, index) => {
        const disabledAttr = option.disabled ? "disabled" : "";
        const descriptionAttr = option.description
          ? `data-description="${option.description}"`
          : "";
        return `
          <div class="option">
            <button ${disabledAttr} data-button="${index}" ${descriptionAttr}>
              ${option.label}
            </button>
            <span class="right">${option.right ? option.right() : ""}</span>
          </div>
        `;
      })
      .join("");

    // Add event listeners to buttons
    this.element.querySelectorAll("button").forEach((button) => {
      button.addEventListener("click", () => {
        const chosenOption = this.options[Number(button.dataset.button)];
        chosenOption.handler();
      });
      button.addEventListener("mouseenter", () => {
        button.focus();
      });
      button.addEventListener("focus", () => {
        this.prevFocus = button;
        const description = button.dataset.description;
        if (description) {
          this.descriptionElementText.innerText = description;
          this.descriptionElement.style.display = "block"; // Show the description box
        } else {
          this.descriptionElement.style.display = "none"; // Hide the description box
        }
      });
    });

    // Set focus on the first non-disabled button
    setTimeout(() => {
      this.element.querySelector("button[data-button]:not([disabled])").focus();
    }, 10);
  }

  /**
   * Create the menu element and the description box element.
   */
  createElement() {
    this.element = document.createElement("div");
    this.element.classList.add("Menu");

    // Description box element
    this.descriptionElement = document.createElement("div");
    this.descriptionElement.classList.add("DescriptionBox");
    this.descriptionElement.innerHTML = `<p>I will provide information!</p>`;
    this.descriptionElementText = this.descriptionElement.querySelector("p");
  }

  /**
   * End the menu by removing elements and cleaning up event listeners.
   */
  end() {
    // Remove menu element and description element
    this.element.remove();
    this.descriptionElement.remove();

    // Clean up bindings
    this.up.unbind();
    this.down.unbind();
  }

  /**
   * Initialize the menu, append it to the container, and set up keyboard navigation.
   * @param {HTMLElement} container - The container to which the menu will be appended.
   */
  init(container) {
    this.createElement();
    (this.descriptionContainer || container).appendChild(
      this.descriptionElement
    );
    container.appendChild(this.element);

    // Set up "ArrowUp" key listener
    this.up = new KeyPressListener("ArrowUp", () => {
      const current = Number(this.prevFocus.getAttribute("data-button"));
      const prevButton = Array.from(
        this.element.querySelectorAll("button[data-button]")
      )
        .reverse()
        .find((el) => {
          return el.dataset.button < current && !el.disabled;
        });
      prevButton?.focus();
    });

    // Set up "ArrowDown" key listener
    this.down = new KeyPressListener("ArrowDown", () => {
      const current = Number(this.prevFocus.getAttribute("data-button"));
      const nextButton = Array.from(
        this.element.querySelectorAll("button[data-button]")
      ).find((el) => {
        return el.dataset.button > current && !el.disabled;
      });
      nextButton?.focus();
    });
  }
}
