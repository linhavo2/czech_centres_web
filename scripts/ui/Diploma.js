/**
 * Diploma class to represent achievement certificates awarded for completing game rooms.
 * Provides details about the achievement and related game company.
 */
class Diploma {
  constructor(nextMap) {
    this.nextMap = nextMap;

    // Configure diploma details based on the next map
    switch (this.nextMap) {
      case "hall_0":
        this.name = utils.getTranslation("start");
        this.logo = "./assets/logos/charles.png";
        this.praise = utils.getTranslation(
          "Your ideas are now nearly as good as in Charles games!"
        );
        this.company = "Charles games";
        this.link =
          "https://infiniteczechgames.com/en/video/video-04-charles-games/";
        break;
      case "hall_1":
        this.name = utils.getTranslation("game design");
        this.logo = "./assets/logos/hangar.svg";
        this.praise = utils.getTranslation(
          "Your storytelling is now like they do in Hangar 13!"
        );
        this.company = "Hangar 13";
        this.link =
          "https://infiniteczechgames.com/cs/video/rozhovor-hangar13/";
        break;
      case "hall_2":
        this.name = utils.getTranslation("visual");
        this.logo = "./assets/logos/amanita.webp";
        this.praise = utils.getTranslation(
          "Wow! Your visuals now almost surpass Amanita Design!"
        );
        this.company = "Amanita Design";
        this.link =
          "https://infiniteczechgames.com/en/video/interview-amanita-design/";
        break;
      case "hall_3":
        this.name = utils.getTranslation("audio");
        this.logo = "./assets/logos/beat.svg";
        this.praise = utils.getTranslation(
          "Your work with audio is almost as good as in Beat Games!"
        );
        this.company = "Beat Saber";
        this.link = "https://infiniteczechgames.com/en/game/beat-saber/";
        break;
      case "hall_4":
        this.name = utils.getTranslation("programming");
        this.logo = "./assets/logos/factorio.png";
        this.praise = utils.getTranslation(
          "You could beat Factorio under 1:21:56 like nothing now!"
        );
        this.company = "Factorio";
        this.link = "https://infiniteczechgames.com/en/game/factorio/";
        break;
      case "hall_5":
        this.name = utils.getTranslation("testing");
        this.logo = "./assets/logos/keen.svg";
        this.praise = utils.getTranslation(
          "Keen Software House and you know something <br> about proper testing!"
        );
        this.company = "Keen Software House";
        this.link =
          "https://infiniteczechgames.com/en/video/video-03-keen-software-house/";
        break;
      case "hall_6":
        this.name = utils.getTranslation("marketing");
        this.logo = "./assets/logos/warhorse.svg";
        this.praise = utils.getTranslation(
          "Everybody will know about your games now, <br> same as about games from Warhorse Studios!"
        );
        this.company = "Warhorse Studios";
        this.link =
          "https://infiniteczechgames.com/en/video/interview-warhorse-studios/";
        break;
      case "room_8":
        this.name = utils.getTranslation("publishing");
        this.logo = "./assets/logos/bohemia.svg";
        this.praise = utils.getTranslation(
          "You could make an incubator<br>like they have in Bohemia Interactive!"
        );
        this.company = "Bohemia Interactive";
        this.link =
          "https://infiniteczechgames.com/en/video/interview-bohemia-interactive/";
        break;
      case "end":
        this.name = utils.getTranslation("end");
        this.logo = "./assets/logos/scs.png";
        this.praise = utils.getTranslation(
          "You know what to do after release of your game,<br>as they do in SCS Software!"
        );
        this.company = "Euro Truck Simulator 2";
        this.link =
          "https://infiniteczechgames.com/cs/hra/euro-truck-simulator-2/";
        break;
      default:
        this.name = utils.getTranslation("Unknown room");
    }
  }

  /**
   * Get the options for the diploma menu.
   * @param {function} resolve - The resolve function to complete the diploma process.
   * @returns {Array} - Array of menu options.
   */
  getOptions(resolve) {
    return [
      {
        label: utils.getTranslation("Continue"),
        handler: () => {
          this.close();
          resolve();
        },
      },
    ];
  }

  /**
   * Create the diploma element and set its inner HTML.
   */
  createElement() {
    this.element = document.createElement("div");
    this.element.classList.add("Diploma");
    this.element.innerHTML = `
      <img class="Diploma_comp_logo" src="${this.logo}" alt="${
      this.company
    } logo" /> 
      <p class="Diploma_praise">${this.praise}</p>
      <p class="Diploma_title">
      ${utils.getTranslation("You have successfully completed room about")} 
        ${this.name} ${utils.getTranslation("in game development")}!
      </p>
      <p class="Diploma_company">${utils.getTranslation("More about")} 
      <a href=${this.link} target="_blank">${this.company}</a>.</p>
    `;
  }

  /**
   * Close the diploma and remove its element from the DOM.
   */
  close() {
    this.menu.end();
    this.element.remove();
  }

  /**
   * Initialize the diploma, append it to the container, and set up the menu.
   * @param {HTMLElement} container - The container to which the diploma will be appended.
   * @returns {Promise} - A promise that resolves when the diploma process is complete.
   */
  init(container) {
    return new Promise((resolve) => {
      this.createElement();
      container.appendChild(this.element);

      this.menu = new Menu();
      this.menu.init(this.element);
      this.menu.setOptions(this.getOptions(resolve));
    });
  }
}
