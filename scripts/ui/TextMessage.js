/**
 * TextMessage class to display text messages with a reveal effect.
 * Allows for completing the message display and executing a callback when done.
 */
class TextMessage {
  constructor({ text, onComplete }) {
    this.text = text; // The text to display
    this.onComplete = onComplete; // Callback to execute when the message is complete
    this.element = null; // The text message element
  }

  /**
   * Create the text message element and set up its content and event listeners.
   */
  createElement() {
    // Create the element
    this.element = document.createElement("div");

    // Add the class
    this.element.classList.add("TextMessage");

    // Add the text and button
    this.element.innerHTML = `
        <p class="TextMessage_p"></p>
        <button class="TextMessage_button">${utils.getTranslation(
          "Next"
        )}</button>
    `;

    // Initialize the text reveal
    this.textReveal = new TextReveal({
      element: this.element.querySelector(".TextMessage_p"),
      text: this.text,
      speed: 50,
    });

    // Add event listener for the button to close the message
    this.element.querySelector("button").addEventListener("click", () => {
      this.done();
    });

    // Add key press listeners for action keys to close the message
    this.actionListeners = utils.actionsKeys.map((key) => {
      const listener = new KeyPressListener(key, () => {
        this.done(listener);
      });
      return listener;
    });
  }

  /**
   * Handle the completion of the text message.
   * @param {KeyPressListener} [listener] - The key press listener to unbind (optional).
   */
  done(listener) {
    if (this.textReveal.isDone) {
      // If the text reveal is done, remove the element and unbind the listener
      this.element.remove();
      if (listener) listener.unbind();
      this.onComplete();
    } else {
      // If the text reveal is not done, complete it immediately
      this.textReveal.toDone();
    }
  }

  /**
   * Initialize the text message, append it to the container, and start the text reveal.
   * @param {HTMLElement} container - The container to which the text message will be appended.
   */
  init(container) {
    this.createElement();
    container.appendChild(this.element);
    this.textReveal.init();
  }
}
