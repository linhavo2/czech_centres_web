/**
 * PauseMenu class to handle the pause menu in the game.
 * Provides options to close the menu and change the language.
 */
class PauseMenu {
  constructor({ onComplete }) {
    this.onComplete = onComplete; // Callback to execute when the menu is closed

    this.menuName = "PauseMenu"; // Default menu name
  }

  /**
   * Get the translated menu name.
   * @returns {string} - The translated menu name.
   */
  getMenuName() {
    return utils.getTranslation(this.menuName);
  }

  /**
   * Get the options for the menu based on the current page.
   * @param {string} pageKey - The key of the current menu page.
   * @returns {Array} - Array of menu options.
   */
  getOptions(pageKey) {
    switch (pageKey) {
      case "root":
        this.changeMenuName("Pause Menu");
        return [
          {
            label: utils.getTranslation("Close"),
            description: utils.getTranslation("Return to the game"),
            handler: () => {
              this.close();
            },
          },
          {
            label: utils.getTranslation("Language"),
            description: utils.getTranslation("Change the language"),
            handler: () => {
              this.menu.setOptions(this.getOptions("language"));
            },
          },
        ];
      case "language":
        this.changeMenuName("Language");
        return [
          {
            label: "English",
            handler: () => {
              utils.setLanguage("English");
              this.menu.setOptions(this.getOptions("root"));
            },
          },
          {
            label: "Čeština",
            handler: () => {
              utils.setLanguage("Czech");
              this.menu.setOptions(this.getOptions("root"));
            },
          },
        ];
    }
  }

  /**
   * Change the menu name and update the display.
   * @param {string} name - The new menu name.
   */
  changeMenuName(name) {
    this.element.querySelector("h2").textContent = utils.getTranslation(name);
  }

  /**
   * Create the menu element and set its inner HTML.
   */
  createElement() {
    this.element = document.createElement("div");
    this.element.classList.add("PauseMenu");
    this.element.innerHTML = `<h2>${this.getMenuName()}</h2>`;
  }

  /**
   * Close the pause menu, remove elements, and execute the onComplete callback.
   */
  close() {
    this.esc?.unbind(); // Unbind the escape key listener if it exists
    this.menu.end(); // End the menu
    this.element.remove(); // Remove the menu element
    this.onComplete(); // Execute the onComplete callback
  }

  /**
   * Initialize the pause menu, append it to the container, and set up the menu options.
   * @param {HTMLElement} container - The container to which the pause menu will be appended.
   */
  init(container) {
    this.createElement();
    this.menu = new Menu({
      descriptionContainer: container,
    });

    this.menu.init(this.element);
    this.menu.setOptions(this.getOptions("root"));

    container.appendChild(this.element);

    utils.wait(200); // Wait for 200 milliseconds before adding the escape key listener
    this.esc = new KeyPressListener("Escape", () => {
      this.close();
    });
  }
}
