/**
 * SceneTransition class to handle transitions between game scenes.
 * Manages fading effects and diploma creation for certain scenes.
 */
class SceneTransition {
  constructor(config) {
    this.nextMap = config.nextMap; // The map to transition to next
    this.element = null; // The transition element
    this.diploma = null; // The diploma element
  }

  /**
   * Create a diploma element and append it to the game container.
   */
  async createDiploma() {
    this.diploma = new Diploma(this.nextMap);
    await this.diploma.init(document.querySelector(".game-container"));
  }

  /**
   * Create the scene transition element.
   */
  createElement() {
    this.element = document.createElement("div");
    this.element.classList.add("SceneTransition");
  }

  /**
   * Fade out the transition element and remove it after the animation ends.
   */
  fadeOut() {
    this.element.classList.add("fade-out");
    this.element.addEventListener(
      "animationend",
      () => {
        this.element.remove();
      },
      { once: true }
    );
  }

  /**
   * Initialize the scene transition, append it to the container, and handle the transition.
   * @param {HTMLElement} container - The container to which the transition element will be appended.
   * @param {function} callback - The callback function to execute after the transition.
   */
  async init(container, callback) {
    this.createElement();
    container.appendChild(this.element);

    this.element.addEventListener(
      "animationend",
      () => {
        // Create diploma if transitioning to a hall or room 8
        if (this.nextMap.includes("hall") || this.nextMap.includes("8")) {
          this.createDiploma();
        }

        callback();
      },
      { once: true }
    );
  }
}
