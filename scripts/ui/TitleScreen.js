/**
 * TitleScreen class to display the title screen of the game.
 * Provides options to start the game or change the language.
 */
class TitleScreen {
  constructor({ onComplete }) {
    this.onComplete = onComplete; // Callback to execute when the title screen is complete
  }

  /**
   * Get the options for the title screen menu.
   * @param {function} resolve - The resolve function to complete the title screen process.
   * @returns {Array} - Array of menu options.
   */
  getOptions(resolve) {
    return [
      {
        label: utils.getTranslation("Start game"),
        handler: () => {
          this.close();
          resolve();
        },
      },
      {
        label: utils.getTranslation("Language"),
        handler: () => {
          // Set options to switch language
          this.menu.setOptions([
            {
              label: "English",
              handler: () => {
                utils.setLanguage("English");
                this.menu.setOptions(this.getOptions(resolve));
              },
            },
            {
              label: "Čeština",
              handler: () => {
                utils.setLanguage("Czech");
                this.menu.setOptions(this.getOptions(resolve));
              },
            },
          ]);
        },
      },
    ];
  }

  /**
   * Create the title screen element and set its inner HTML.
   */
  createElement() {
    this.element = document.createElement("div");
    this.element.classList.add("TitleScreen");
    this.element.innerHTML = `
      <img class="TitleScreen_logo" src="assets/logos/cc/bigLogoPNG.png" alt="Česká centra logo" />
    `;
  }

  /**
   * Close the title screen, remove elements, and end the menu.
   */
  close() {
    this.menu.end();
    this.element.remove();
  }

  /**
   * Initialize the title screen, append it to the container, and set up the menu options.
   * @param {HTMLElement} container - The container to which the title screen will be appended.
   * @returns {Promise} - A promise that resolves when the title screen process is complete.
   */
  init(container) {
    return new Promise((resolve) => {
      this.createElement();
      container.appendChild(this.element);

      this.menu = new Menu();
      this.menu.init(this.element);
      this.menu.setOptions(this.getOptions(resolve));
    });
  }
}
