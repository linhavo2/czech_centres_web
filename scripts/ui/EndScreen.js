/**
 * EndScreen class to display the end screen of the game.
 * Provides options to view credits or close the screen.
 */
class EndScreen {
  constructor({ onComplete }) {
    this.container = null; // The container to append the end screen to
  }

  /**
   * Get the options for the end screen menu.
   * @param {function} resolve - The resolve function to complete the process.
   * @returns {Array} - Array of menu options.
   */
  getOptions(resolve) {
    return [
      {
        label: utils.getTranslation("Credits"),
        handler: () => {
          this.close();
          this.creditsScreen = new CreditsScreen({});
          this.creditsScreen.init(this.container);
        },
      },
      {
        label: utils.getTranslation("End"),
        handler: () => {
          this.close();
          this.blueScreen = new BlueScreen({});
          this.blueScreen.init(this.container);
        },
      },
    ];
  }

  /**
   * Create the end screen element and set its inner HTML.
   */
  createElement() {
    this.element = document.createElement("div");
    this.element.classList.add("EndScreen");
    this.element.innerHTML = `
      <img class="EndScreen_logo" src="assets/logos/cc/bigLogoPNG.png" alt="Česká centra logo" />
      <h1 class="EndScreen_title">${utils.getTranslation(
        "CONGRATULATIONS!"
      )}</h1>
      <hr>
      <p class="EndScreen_text">${utils.getTranslation(
        "You now know about game development everything from A to Z. Almost..."
      )}</p>
      <p class="EndScreen_more">${utils.getTranslation(
        "If you want to know more, visit"
      )} 
      <a href="https://infiniteczechgames.com" target="_blank">Infinite Universes</a>.</p>
    `;
  }

  /**
   * Close the end screen and remove its element from the DOM.
   */
  close() {
    this.menu.end();
    this.element.remove();
  }

  /**
   * Initialize the end screen, append it to the container, and set up the menu.
   * @param {HTMLElement} container - The container to which the end screen will be appended.
   * @returns {Promise} - A promise that resolves when the end screen process is complete.
   */
  init(container) {
    this.container = container;

    return new Promise((resolve) => {
      this.createElement();
      container.appendChild(this.element);

      this.menu = new Menu();
      this.menu.init(this.element);
      this.menu.setOptions(this.getOptions(resolve));
    });
  }
}

/**
 * BlueScreen class to display a blue screen message.
 * Provides an option to close the screen.
 */
class BlueScreen {
  constructor({ onComplete }) {}

  /**
   * Create the blue screen element and set its inner HTML.
   */
  createElement() {
    this.element = document.createElement("div");
    this.element.classList.add("BlueScreen");
    this.element.innerHTML = `
      <p class="BlueScreen_emoji">:(</p>
      <p class="BlueScreen_text">${utils.getTranslation(
        "You have completed all there is."
      )}</p>
      <p class="BlueScreen_more">${utils.getTranslation(
        "If you want to know more, visit"
      )} 
      <a href="https://infiniteczechgames.com" target="_blank">Infinite Universes</a>.</p>
      <p class="BlueScreen_src">${utils.getTranslation(
        "Or if you want to see how this game was made, visit"
      )}
      <a href="https://gitlab.fel.cvut.cz/linhavo2/czech_centres_web" target="_blank">GitLab</a>.</p>
      <button class="BlueScreen_button" onClick="history.go(0);">${utils.getTranslation(
        "End"
      )}</button>
    `;
  }

  /**
   * Close the blue screen and remove its element from the DOM.
   */
  close() {
    this.menu.end();
    this.element.remove();
  }

  /**
   * Initialize the blue screen, append it to the container, and set up the menu.
   * @param {HTMLElement} container - The container to which the blue screen will be appended.
   * @returns {Promise} - A promise that resolves when the blue screen process is complete.
   */
  init(container) {
    return new Promise(() => {
      this.createElement();
      container.appendChild(this.element);
    });
  }
}

/**
 * CreditsScreen class to display the credits screen.
 * Provides an option to go back to the end screen.
 */
class CreditsScreen {
  constructor({ onComplete }) {
    this.container = null; // The container to append the credits screen to
  }

  /**
   * Get the options for the credits screen menu.
   * @param {function} resolve - The resolve function to complete the process.
   * @returns {Array} - Array of menu options.
   */
  getOptions(resolve) {
    return [
      {
        label: utils.getTranslation("Back"),
        handler: () => {
          this.close();
          this.endScreen = new EndScreen({});
          this.endScreen.init(this.container);
        },
      },
    ];
  }

  /**
   * Create the credits screen element and set its inner HTML.
   */
  createElement() {
    this.element = document.createElement("div");
    this.element.classList.add("CreditsScreen");
    this.element.innerHTML = `
      <h1>${utils.getTranslation("Credits")}</h1>
      <hr>
      <h2>${utils.getTranslation("Author")}</h2>
      <p>Vojtěch Linha</p>
      <h2>${utils.getTranslation("Base for sprites")}</h2>
      <p>
        <a href="https://anokolisa.itch.io/dungeon-crawler-pixel-art-asset-pack" target="_blank">
          ${utils.getTranslation("Background")}
        </a> <br>
        <a href="https://totuslotus.itch.io/characterpack" target="_blank">
          ${utils.getTranslation("Character")}
        </a>
      </p>
      <h2>${utils.getTranslation("Audio")}</h2>
      <p>
        <a href="https://pixabay.com/cs/sound-effects/" target="_blank">Pixabay</a>
      </p>
    `;
  }

  /**
   * Close the credits screen and remove its element from the DOM.
   */
  close() {
    this.menu.end();
    this.element.remove();
  }

  /**
   * Initialize the credits screen, append it to the container, and set up the menu.
   * @param {HTMLElement} container - The container to which the credits screen will be appended.
   * @returns {Promise} - A promise that resolves when the credits screen process is complete.
   */
  init(container) {
    this.container = container;

    return new Promise(() => {
      this.createElement();
      container.appendChild(this.element);

      this.menu = new Menu();
      this.menu.init(this.element);
      this.menu.setOptions(this.getOptions());
    });
  }
}
