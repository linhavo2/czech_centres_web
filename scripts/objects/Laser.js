/**
 * Laser class extends GameObject to represent a laser obstacle in the game.
 * The laser can toggle its active state at regular intervals and has a configurable
 * image source and size.
 */
class Laser extends GameObject {
  constructor(config) {
    super(config);

    // Set the initial active state of the laser, defaulting to false if not provided
    this.active = config.active || false;

    // Set the source image for the laser and a transparent image
    this.src = config.src || "./assets/obstacles/laser/1.png";
    this.transparent = "./assets/obstacles/laser/transparent.png";

    // Define the size of the laser
    this.size = {
      width: 6,
      height: 109,
    };

    // Initialize a counter to track updates
    this.counter = 0;
  }

  /**
   * Method to check if the laser is active.
   * @returns {boolean} - The active state of the laser.
   */
  isActive() {
    return this.active;
  }

  /**
   * Method to update the laser's state.
   * Toggles the active state of the laser every 80 updates.
   */
  update() {
    this.counter++;
    // Toggle the active state of the laser every 80 updates
    if (this.counter % 80 === 0) {
      this.active = !this.active;
    }
  }
}
