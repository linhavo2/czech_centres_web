/**
 * GameObject class to represent entities in the game with position, direction, and sprite.
 * Handles behavior events and interaction with the game map.
 */
class GameObject {
  constructor(config) {
    this.id = null; // Unique identifier for the game object
    this.position = config.position || { x: 0, y: 0 }; // Position of the game object
    this.direction = config.direction || "right"; // Initial direction of the game object

    // Offset for rendering the game object
    this.offset = config.offset || { x: 0, y: 0 };

    // Set up the sprite with configuration
    this.sprite = new Sprite({
      gameObject: this,
      src: config.src || "./assets/my_take-Sheet.png",
      useShadow: config.useShadow || false,
      useAnim: config.useAnim == undefined || config.useAnim || false,
    });

    // Behavior loop configuration
    this.behaviorLoop = config.behaviorLoop || [];
    this.behaviorIndex = 0;

    // Talking state configuration
    this.talking = config.talking || [];
    this.visible = config.visible === undefined || config.visible;

    // Steps sound configuration
    this.steps = new StepSound({ useSound: config.useSound || false });
  }

  /**
   * Mount the game object to the map and start its behavior loop.
   * @param {object} map - The current game map.
   */
  mount(map) {
    this.steps.mount(this.id); // Mount the step sound
    setTimeout(() => {
      this.doBehaviorEvent(map); // Start behavior after a short delay
    }, 10);
  }

  /**
   * Update method to be overridden by subclasses for specific behavior.
   */
  update() {}

  /**
   * Switch the sprite of the game object.
   * @param {object} config - Configuration for the new sprite.
   */
  switchSprite(config) {
    this.sprite = new Sprite({
      gameObject: this,
      src: config.src,
      useShadow: config.useShadow,
      useAnim: config.useAnim,
    });
  }

  /**
   * Execute the behavior event loop for the game object.
   * @param {object} map - The current game map.
   */
  async doBehaviorEvent(map) {
    if (map.isCutScenePlaying || this.behaviorLoop.length === 0) {
      // If cutscene is playing or no behavior loop, wait
      return;
    }

    // Set the current behavior event
    let eventConfig = this.behaviorLoop[this.behaviorIndex];
    eventConfig.who = this.id;

    // Create and initialize the event handler
    const eventHandler = new WorldEvent({ map, event: eventConfig });
    await eventHandler.init(); // Wait until the event is completed

    // Move to the next behavior in the loop
    this.behaviorIndex++;
    if (this.behaviorIndex === this.behaviorLoop.length) {
      this.behaviorIndex = 0;
    }

    // Continue the behavior loop
    this.doBehaviorEvent(map);
  }
}
