/**
 * Person class extends GameObject to represent a person in the game.
 * The person can be controlled by the player or AI, move in different directions,
 * and interact with the environment via hitbox checks.
 */
class Person extends GameObject {
  constructor(config) {
    super(config);

    // Determine if the person is controlled by the player
    this.isPlayerControlled = config.isPlayerControlled || false;

    // Set the initial direction of the person, defaulting to "down"
    this.direction = config.direction || "down";

    // Mapping of directions to their corresponding coordinate changes
    this.directionUpdate = {
      up: ["y", -1],
      down: ["y", 1],
      left: ["x", -1],
      right: ["x", 1],
    };

    // Movement speed in pixels per move
    this.speed = config.speed || 1;

    // Size of the person object
    this.size = config.size || { width: 12, height: 14 };

    // Offset for rendering the person object
    this.offset = config.offset || { x: 9, y: 13 };

    // Flag to determine if half the height should be used for hitbox checks
    this.dontUseHalfY = config.dontUseHalfY || false;

    // Track the progress of movement
    this.movingProgress = 0;
  }

  /**
   * Update the state of the person based on player input or AI behavior.
   * @param {object} state - The current state of the game.
   */
  update(state) {
    this.hitboxCheck(state, this.position);

    if (this.isPlayerControlled && this.movingProgress === 0) {
      // Adjust speed based on shift key press
      this.speed = state.input.shiftPressed ? 2 : 1;

      if (state.input.direction && !state.map.isCutScenePlaying) {
        // Update direction and position based on input
        this.direction = state.input.direction;
        let newPosition = this.newPosition();
        if (this.hitboxCheck(state, newPosition)) {
          this.updateSprite("walk");
          this.position = newPosition;
        }
        this.steps.play();
      } else {
        this.updateSprite("idle");
      }
    } else {
      if (this.movingProgress > 0) {
        // Continue movement if in progress
        let newPosition = this.newPosition();
        if (this.hitboxCheck(state, newPosition)) {
          this.position = newPosition;
          this.movingProgress--;
          this.updateSprite("walk");
          this.steps.play();
        } else {
          this.updateSprite("idle");
        }

        if (this.movingProgress === 0) {
          utils.emitEvent("BehaviorWalkComplete", { whoId: this.id });
        }
      } else {
        this.updateSprite("idle");
      }
    }
  }

  /**
   * Check for collisions based on the person's hitbox.
   * @param {object} state - The current state of the game.
   * @param {object} position - The position to check for collisions.
   * @returns {boolean} - Whether the position is free of collisions.
   */
  hitboxCheck(state, position) {
    let addedHeight = this.dontUseHalfY ? 0 : this.size.height / 2;

    const corners = {
      left_up: { x: position.x, y: position.y + addedHeight },
      right_up: {
        x: position.x + this.size.width,
        y: position.y + addedHeight,
      },
      right_down: {
        x: position.x + this.size.width,
        y: position.y + this.size.height,
      },
      left_down: { x: position.x, y: position.y + this.size.height },
    };

    const halfSizeY = {
      width: this.size.width,
      height: this.dontUseHalfY ? this.size.height : this.size.height / 2,
    };

    // Check if there is a collision with another object
    if (state.map.pixelCollision(corners.left_up, halfSizeY, this.id))
      return false;

    // Check if there is a collision with grid-like objects (walls)
    let currentCorner = 0;
    for (let corner in corners) {
      if (
        state.map.gridCollision(
          corners[corner],
          this.id === "player",
          currentCorner
        )
      )
        return false;
      currentCorner++;
    }

    return true;
  }

  /**
   * Calculate the new position based on the current direction and speed.
   * @returns {object} - The new position.
   */
  newPosition() {
    const [property, change] = this.directionUpdate[this.direction];
    const x = this.position.x;
    const y = this.position.y;

    let newPosition = { x, y };

    newPosition[property] =
      this.position[property] + Math.round(change * this.speed);
    return newPosition;
  }

  /**
   * Start a specific behavior (idle or walk) for the person.
   * @param {object} state - The current state of the game.
   * @param {object} behavior - The behavior to start.
   */
  startBehavior(state, behavior) {
    this.direction = behavior.direction;

    if (behavior.type === "idle") {
      this.updateSprite("idle");

      setTimeout(() => {
        utils.emitEvent("BehaviorIdleComplete", { whoId: this.id });
      }, behavior.duration);
    }

    if (behavior.type === "walk") {
      this.movingProgress = utils.tileToPixel(behavior.steps);
    }
  }

  /**
   * Update the sprite animation based on the current type (idle or walk).
   * @param {string} type - The type of animation to set.
   */
  updateSprite(type) {
    if (type === "idle") {
      this.sprite.setAnimation("idle-" + this.direction);
      return;
    }

    if (type === "walk") {
      this.sprite.setAnimation("walk-" + this.direction);
      return;
    }
  }
}
