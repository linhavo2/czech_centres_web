/**
 * Collectible class to represent objects that can be collected to unlock a door.
 * Extends GameObject to inherit position, visibility, and sprite properties.
 */
class Collectible extends GameObject {
  constructor(config) {
    super(config);

    // Set the offset position for the collectible
    this.offset = config.offset || { x: 0, y: 0 };

    // Set the size of the collectible
    this.size = config.size || { width: 0, height: 0 };

    // Flag to track if the collectible has been collected
    this.isCollected = false;

    // Set the initial animation state
    this.sprite.setAnimation("idle-down");
  }

  /**
   * Collect the item and change its appearance.
   */
  collect() {
    this.isCollected = true; // Mark the collectible as collected
    this.switchSprite({ src: "./assets/obstacles/collectibles/key_0.png" }); // Change the sprite to indicate collection
  }
}
