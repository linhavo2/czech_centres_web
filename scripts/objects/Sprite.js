// Definitions
const defaultFrameDuration = 8;

/**
 * Sprite class to represent and animate game sprites.
 * Handles loading images, animations, and drawing sprites on the canvas.
 */
class Sprite {
  constructor(config) {
    // Set up the sprite image
    this.image = new Image();
    this.image.src = config.src;
    this.image.onload = () => {
      this.isLoaded = true;
    };

    // Determine if the sprite should use animations
    this.useAnim = config.useAnim || false;

    // Set up shadow if required
    this.useShadow = config.useShadow || false;
    if (this.useShadow) {
      this.shadow = new Image();
      this.shadow.src = "./assets/shadow/shadow.png";
      this.shadow.onload = () => {
        this.shadow.isLoaded = true;
      };
    }

    // Configure sprite animations
    this.animations = config.animations || {
      "idle-right": [
        [4, 0],
        [4, 0],
        [5, 0],
        [5, 0],
      ],
      "idle-left": [
        [6, 0],
        [6, 0],
        [7, 0],
        [7, 0],
      ],
      "idle-down": [
        [0, 0],
        [0, 0],
        [1, 0],
        [1, 0],
      ],
      "idle-up": [
        [2, 0],
        [2, 0],
        [3, 0],
        [3, 0],
      ],
      "walk-right": [
        [8, 1],
        [9, 1],
        [10, 1],
        [11, 1],
      ],
      "walk-left": [
        [12, 1],
        [13, 1],
        [14, 1],
        [15, 1],
      ],
      "walk-down": [
        [0, 1],
        [1, 1],
        [2, 1],
        [3, 1],
      ],
      "walk-up": [
        [4, 1],
        [5, 1],
        [6, 1],
        [7, 1],
      ],
    };

    this.currentAnimation = config.currentAnimation || "idle-down"; // Current animation loop
    this.currentAnimationFrame = 0; // Current frame of the animation

    this.frameDuration = config.frameDuration || defaultFrameDuration; // Frames to wait before changing the sprite
    this.frameProgress = this.frameDuration; // Frames currently waited before changing the sprite

    // Reference to the associated GameObject
    this.gameObject = config.gameObject;
  }

  /**
   * Get the current frame of the animation.
   * @returns {array} - The current frame [x, y].
   */
  get frame() {
    return this.animations[this.currentAnimation][this.currentAnimationFrame];
  }

  /**
   * Set the current animation.
   * @param {string} key - The animation key.
   */
  setAnimation(key) {
    if (this.currentAnimation === key) return;

    this.currentAnimation = key;
    this.currentAnimationFrame = 0;
    this.frameProgress = this.frameDuration;
  }

  /**
   * Update the animation progress and switch frames if needed.
   */
  updateAnim() {
    if (this.frameProgress > 0) {
      this.frameProgress--;
      return;
    }

    this.frameProgress = this.frameDuration;
    this.currentAnimationFrame++;

    if (this.frame === undefined) {
      this.currentAnimationFrame = 0;
    }
  }

  /**
   * Draw the sprite on the canvas.
   * @param {object} ctx - The canvas rendering context.
   * @param {object} camera - The camera object to adjust the drawing position.
   */
  draw(ctx, camera) {
    if (!this.gameObject.visible) return;

    // Special handling for Laser objects
    if (this.gameObject instanceof Laser) {
      if (!this.gameObject.isActive()) {
        return;
      }

      // Set the position of the sprite
      const x =
        this.gameObject.position.x -
        this.gameObject.offset.x +
        utils.map.center.x -
        camera.position.x;
      const y =
        this.gameObject.position.y -
        this.gameObject.offset.y +
        utils.map.center.y -
        camera.position.y;

      this.isLoaded && ctx.drawImage(this.image, x, y);
      return;
    }

    // Set the position of the sprite
    const x =
      this.gameObject.position.x -
      this.gameObject.offset.x +
      utils.map.center.x -
      camera.position.x;
    const y =
      this.gameObject.position.y -
      this.gameObject.offset.y +
      utils.map.center.y -
      camera.position.y;

    // Draw the shadow if enabled
    this.useShadow && this.shadow.isLoaded && ctx.drawImage(this.shadow, x, y);

    let fX, fY;
    if (this.useAnim) {
      const [frameX, frameY] = this.frame; // Current frame of the animation
      fX = frameX;
      fY = frameY;
    } else {
      fX = 0;
      fY = 0;
    }

    // Draw the sprite image
    this.isLoaded &&
      ctx.drawImage(this.image, fX * 32, fY * 32, 32, 32, x, y, 32, 32); // Same sizes/cuts of sprites. Every sheet is 32x32

    this.updateAnim();
  }
}
