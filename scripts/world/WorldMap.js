/**
 * WorldMap class to manage the game world map, including layers, game objects, and events.
 */
class WorldMap {
  constructor(config) {
    this.gameObjects = config.gameObjects; // Game objects in the map
    this.walls = config.walls || {}; // Map walls for collision detection

    // Lower layer
    this.lowerImage = new Image();
    this.lowerImage.src = config.lowerSrc;
    this.lowerImage.onload = () => {
      this.lowerImage.isLoaded = true;
    };

    // Upper layer
    this.upperImage = new Image();
    this.upperImage.src = config.upperSrc || "";
    if (this.upperImage.src) {
      this.upperImage.onload = () => {
        this.upperImage.isLoaded = true;
      };
    }

    this.triggeredEvents = new Set(); // Set of triggered events to avoid repetition

    this.isPaused = false; // Flag to check if the map is paused

    this.coll = {
      collectibles: config.collectibles || {}, // Collectible items in the map
      index: 0, // Index to track collected items
    };

    this.nextRooms = config.nextRooms || {}; // Configuration for switching rooms

    this.useDiploma = config.useDiploma === undefined || config.useDiploma; // Flag to use diploma

    this.isCutScenePlaying = false; // Flag to check if a cutscene is playing
    this.cutsceneSpaces = config.cutsceneSpaces || {}; // Spaces that trigger cutscenes

    this.useMusic = config.useMusic || false; // Flag to use background music
    this.music = new Music(); // Music object

    this.currentTile = [
      { x: 0, y: 0 },
      { x: 0, y: 0 },
      { x: 0, y: 0 },
      { x: 0, y: 0 },
    ]; // Current tile positions

    this.restart = {
      positions: config.restartPositions || [], // Positions to restart from
      index: 0, // Current restart position index
    };
  }

  /**
   * Initialize and play background music if enabled.
   */
  initMusic() {
    if (this.useMusic) {
      this.music.init();
      this.music.play();
    }
  }

  /**
   * Draw the lower layer of the map.
   * @param {CanvasRenderingContext2D} ctx - The canvas rendering context.
   * @param {object} camera - The camera object for positioning.
   */
  drawLower(ctx, camera) {
    this.lowerImage.isLoaded &&
      ctx.drawImage(
        this.lowerImage,
        utils.map.center.x - camera.position.x,
        utils.map.center.y - camera.position.y
      );
  }

  /**
   * Draw the upper layer of the map.
   * @param {CanvasRenderingContext2D} ctx - The canvas rendering context.
   * @param {object} camera - The camera object for positioning.
   */
  drawUpper(ctx, camera) {
    this.upperImage.isLoaded &&
      ctx.drawImage(
        this.upperImage,
        utils.map.center.x - camera.position.x,
        utils.map.center.y - camera.position.y
      );
  }

  /**
   * Start a cutscene and handle each event in the cutscene.
   * @param {Array} events - Array of events to execute in the cutscene.
   */
  async startCutScene(events) {
    this.isCutScenePlaying = true;

    for (let i = 0; i < events.length; i++) {
      const eventHandler = new WorldEvent({
        event: events[i],
        map: this,
      });

      this.isCutScenePlaying = events[i].stopper === false ? false : true;

      await eventHandler.init();
    }

    this.isCutScenePlaying = false;

    Object.values(this.gameObjects).forEach((object) =>
      object.doBehaviorEvent(this)
    );
  }

  /**
   * Check if a position is a wall.
   * @param {object} position - The position to check.
   * @returns {boolean} - True if the position is a wall, otherwise false.
   */
  isWall(position) {
    const { x, y } = utils.posToGrid(position);
    return this.walls[`${x},${y}`];
  }

  /**
   * Check for pixel-based collisions.
   * @param {object} position - The position to check.
   * @param {object} size - The size of the object.
   * @param {string} id - The ID of the object.
   * @returns {boolean} - True if there is a collision, otherwise false.
   */
  pixelCollision(position, size, id) {
    let isTaken = false;

    Object.values(this.gameObjects).forEach((object) => {
      if (id !== object.id) {
        if (
          position.x < object.position.x + object.size.width &&
          position.x + size.width > object.position.x &&
          position.y < object.position.y + object.size.height &&
          position.y + size.height > object.position.y
        ) {
          if (object instanceof Person && object.id.includes("blue"))
            return false;

          if (object instanceof Laser && !object.isActive()) return false;

          if (object instanceof Collectible && object.isCollected) return false;

          if (
            (id === "producer" && object.id === "player") ||
            (id === "player" && object.id === "producer")
          ) {
            console.log("Producer collision");
            this.restartPosition();
            this.startCutScene([
              { type: "despawn", who: "producer" },
              {
                type: "textMessage",
                text: randText(),
                stopper: true,
              },
            ]);

            function randText() {
              const texts = [
                "Deadlines are ruthless. You have to be faster next time!",
                "You can't be late. You have to be faster!",
                "Release date is coming. You have to be faster!",
              ];
              return texts[Math.floor(Math.random() * texts.length)];
            }
          }

          if (id === "player") {
            // Laser
            if (object instanceof Laser && object.isActive()) {
              this.restartPosition();
            }

            // Collectible
            if (
              object.id.includes("key") &&
              !object.isCollected &&
              this.coll.collectibles[this.coll.index] === object.id
            ) {
              this.collecting(object);
            } else if (object.isCollected) return false;
          }

          if (id !== "player" && object.id === "player") {
            isTaken = false;
          }

          isTaken = true;

          // Check if there is an object with cutscene
          if (!this.isCutScenePlaying && object.talking.length) {
            console.log("Starting cutscene:", object.talking[0]);
            this.startCutScene(object.talking);
          }
        }
      }
    });

    return isTaken;
  }

  /**
   * Handle collecting an object.
   * @param {Collectible} object - The collectible object.
   */
  collecting(object) {
    object.collect();
    this.coll.index++;
    if (this.coll.index === Object.keys(this.coll.collectibles).length) {
      console.log("All keys collected");
      this.removeWall(28, 12);
      this.removeWall(28, 13);
      this.switchSprite("lower", "./assets/rooms/5_room/bg.png");
      this.switchSprite("upper", "./assets/rooms/5_room/up.png");
      this.startCutScene([
        {
          type: "walking",
          who: "npc",
          walks: [
            { direction: "right", steps: 9 },
            { direction: "up", steps: 1 },
            { direction: "down", steps: 0 },
          ],
          stopper: false,
          count: 1,
        },
      ]);
    }
  }

  /**
   * Check for grid-based collisions.
   * @param {object} position - The position to check.
   * @param {boolean} isPlayer - Whether the object is the player.
   * @param {number} currentCorner - The current corner index.
   * @returns {boolean} - True if there is a collision, otherwise false.
   */
  gridCollision(position, isPlayer, currentCorner) {
    const { x, y } = utils.posToGrid(position);

    // Wall
    if (this.isWall(position)) return true;

    // Room switch
    if (
      this.nextRooms[`${x},${y}`] !== undefined &&
      !this.triggeredEvents.has(`${x},${y}`)
    ) {
      this.triggeredEvents.add(`${x},${y}`);
      this.switchMap(x, y);
      return true;
    }

    // Cutscene space
    if (
      isPlayer &&
      this.cutsceneSpaces[`${x},${y}`] !== undefined &&
      !this.currentTile.some((tile) => tile.x === x && tile.y === y) &&
      !this.triggeredEvents.has(`${x},${y}`)
    ) {
      this.triggeredEvents.add(`${x},${y}`);
      this.startCutScene(this.cutsceneSpaces[`${x},${y}`]);

      this.restart.position = utils.gridToPos({ x, y });
      this.triggeredEvents.delete(`${x},${y}`);
    }

    // Add current tile to tiles in hold
    if (isPlayer) this.currentTile[currentCorner] = { x, y };
  }

  /**
   * Switch to a different map based on coordinates.
   * @param {number} x - The x coordinate.
   * @param {number} y - The y coordinate.
   */
  switchMap(x, y) {
    console.log("Switching room to:", this.nextRooms[`${x},${y}`]);

    gtag("event", "room_switch", {
      event_category: "Navigation",
      event_label: `From ${this.nextRooms[`${x},${y}`].split("_")[1] - 1} to ${
        this.nextRooms[`${x},${y}`]
      }`,
      value: parseInt(this.nextRooms[`${x},${y}`].split("_")[1]),
    });

    gtag("event", this.nextRooms[`${x},${y}`], {
      event_category: "room_change",
      value: this.nextRooms[`${x},${y}`].split("_")[1],
    });

    const sceneTransition = new SceneTransition({
      nextMap: this.nextRooms[`${x},${y}`],
    });

    if (this.nextRooms[`${x},${y}`] === "end") {
      console.log("Game ended");
      this.world.endGame();
      return;
    }

    sceneTransition.init(document.querySelector(".game-container"), () => {
      this.world.startMap(window.WorldMaps[this.nextRooms[`${x},${y}`]]);
      sceneTransition.fadeOut();
    });
  }

  /**
   * Mount all game objects to the map.
   */
  mountObjects() {
    Object.keys(this.gameObjects).forEach((key) => {
      let object = this.gameObjects[key];
      object.id = key;
      object.mount(this);
    });
  }

  /**
   * Add a wall at the specified grid coordinates.
   * @param {number} x - The x coordinate.
   * @param {number} y - The y coordinate.
   */
  addWall(x, y) {
    this.walls[`${x},${y}`] = true;
  }

  /**
   * Remove a wall at the specified grid coordinates.
   * @param {number} x - The x coordinate.
   * @param {number} y - The y coordinate.
   */
  removeWall(x, y) {
    delete this.walls[`${x},${y}`];
  }

  /**
   * Switch the sprite of the lower or upper layer.
   * @param {string} what - Which layer to switch ("lower" or "upper").
   * @param {string} src - The source of the new sprite.
   */
  switchSprite(what, src) {
    switch (what) {
      case "upper":
        this.upperImage.src = src;
        this.upperImage.onload = () => {
          this.upperImage.isLoaded = true;
        };
        break;
      case "lower":
        this.lowerImage.src = src;
        this.lowerImage.onload = () => {
          this.lowerImage.isLoaded = true;
        };
        break;
    }
  }

  /**
   * Restart the player's position to the last restart position.
   */
  restartPosition() {
    console.log(
      "Restarting position to:",
      this.restart.positions[this.restart.index]
    );
    const restart = utils.gridToPos(this.restart.positions[this.restart.index]);
    this.gameObjects.player.position = restart;
  }
}

// Place for all levels/rooms
/*
Room has:
- lower/upper layer source
- game objects:
  - characters
    - position
    - src
    - speed
    - isPlayerControlled
    - useShadow
*/
window.WorldMaps = {
  room_intro: room_intro,
  room_0: room_0_start,
  room_1: room_1_gd,
  room_2: room_2_vis,
  room_3: room_3_aud,
  room_4: room_4_progr,
  room_5: room_5_test,
  room_6: room_6_mark,
  room_7: room_7_publ,
  room_8: room_8_end,

  hall_0: hall_0,
  hall_1: hall_1,
  hall_2: hall_2,
  hall_3: hall_3,
  hall_4: hall_4,
  hall_5: hall_5,
  hall_6: hall_6,
};
