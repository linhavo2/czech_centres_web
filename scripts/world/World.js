/**
 * World class to manage the game world, including the map, game loop, and player input.
 */
class World {
  constructor(config) {
    this.element = config.element; // The DOM element containing the game
    this.canvas = this.element.querySelector(".game-canvas"); // The canvas element for rendering
    this.ctx = this.canvas.getContext("2d"); // The 2D rendering context for the canvas
    this.map = null; // The current game map
  }

  /**
   * Perform game loop work, including updating and drawing game objects.
   * @param {number} deltaTime - The time since the last frame.
   */
  gameLoopWork(deltaTime) {
    // Handle pause
    if (this.map == null || this.map.isPaused) {
      return;
    }

    // Clear the canvas
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    // Camera
    const camera = this.map.gameObjects.player;

    // Update all objects
    Object.values(this.map.gameObjects).forEach((object) => {
      // Adjust the movement speed using deltaTime
      object.update({
        input: this.input,
        deltaTime: deltaTime,
        map: this.map,
      });
    });

    // Draw the lower part of the map
    this.map.drawLower(this.ctx, camera);

    // Draw all the collectibles first
    Object.values(this.map.gameObjects)
      .filter((object) => object instanceof Collectible)
      .forEach((object) => {
        object.sprite.draw(this.ctx, camera);
      });

    // Draw all the other game objects based on their y coordinate
    Object.values(this.map.gameObjects)
      .filter((object) => !(object instanceof Collectible))
      .sort((a, b) => a.position.y - b.position.y)
      .forEach((object) => {
        object.sprite.draw(this.ctx, camera);
      });

    // Draw the upper part of the map
    this.map.drawUpper(this.ctx, camera);
  }

  /**
   * Start the main game loop.
   */
  startGameLoop() {
    let previousTime;
    const stepFunc = (timestamp) => {
      // Set the previous time if it's not set
      if (previousTime === undefined) {
        previousTime = timestamp;
      }

      // Calculate the delta time
      let deltaTime = Math.min(1, (timestamp - previousTime) / 1000);
      // Update the game state
      while (deltaTime >= utils.defaultFrameRate) {
        this.gameLoopWork(deltaTime);
        deltaTime -= utils.defaultFrameRate;
      }
      // Make sure we don't lose unprocessed time
      previousTime = timestamp - deltaTime * 1000;

      // Request the next frame
      requestAnimationFrame(stepFunc);
    };

    // Start the loop
    requestAnimationFrame(stepFunc);
  }

  /**
   * Start a new map.
   * @param {object} mapConfig - The configuration for the new map.
   */
  startMap(mapConfig) {
    this.map && this.map.music.destroy();

    this.map = new WorldMap(mapConfig);
    this.map.mountObjects();
    this.map.world = this;

    // Play music
    this.map.initMusic();
  }

  /**
   * End the game and display the end screen.
   */
  async endGame() {
    // End the game
    this.diploma = new Diploma("end");
    await this.diploma.init(document.querySelector(".game-container"));

    this.map.music.destroy();
    this.map = null;

    this.endScreen = new EndScreen({});
    await this.endScreen.init(document.querySelector(".game-container"));
    console.log("Game ended");
  }

  /**
   * Bind action input for restarting position and opening the pause menu.
   */
  bindActionInput() {
    new KeyPressListener("KeyR", () => {
      // Restart position
      this.map.restartPosition();
    });

    new KeyPressListener("Escape", () => {
      if (!this.map.isCutScenePlaying) {
        this.map.startCutScene([{ type: "pause" }]);
      }
    });
  }

  /**
   * Initialize the game, display the title screen, and start the first map.
   */
  async init() {
    utils.language = "English";

    const container = document.querySelector(".game-container");

    // Title screen
    this.titleScreen = new TitleScreen({});
    await this.titleScreen.init(container);

    // Load the first map
    this.startMap(window.WorldMaps.room_intro);

    // Bind action input
    this.bindActionInput();

    // Initialize input handling
    this.input = new Input();
    this.input.init();

    // Start the game loop
    this.startGameLoop();

    // Start the initial cutscene
    this.map.startCutScene([
      {
        type: "textMessage",
        text: "Ha, you are finally awake. Hello and welcome to the game! I will be your guide.",
      },
      {
        type: "textMessage",
        text: "What you will experience is a journey through the world of game development.",
      },
      {
        type: "textMessage",
        text: "There are 9 rooms dedicated to fundamental phases of game development.",
      },
      {
        type: "textMessage",
        text: "As you progress through the rooms, you will learn about the game development process and the game will progress with you.",
      },
      {
        type: "textMessage",
        text: "You don't understand? Don't worry, you will see.",
      },
      {
        type: "walking",
        who: "player",
        walks: [
          { direction: "right", steps: 2 },
          { direction: "up", steps: 4 },
        ],
        stopper: false,
        count: 1,
      },
    ]);
  }
}
