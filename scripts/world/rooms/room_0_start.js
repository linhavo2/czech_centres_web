const infoTexts_r0 = {
  inspoArray: [
    {
      type: "textMessage",
      text: "The journey begins with an Idea.",
    },
    {
      type: "textMessage",
      text: "It's like the first spark of a fire, where developers brainstorm different concepts, imagining the kind of game they want to create.",
    },
    {
      type: "textMessage",
      text: "This initial burst of creativity sets the course for everything that follows, igniting the enthusiasm to bring this game to life.",
    },
  ],
  goalsArray: [
    {
      type: "textMessage",
      text: "They then set their Goals, deciding what they aim to achieve with the game.",
    },
    {
      type: "textMessage",
      text: "Whether it's to create an exciting adventure or a compelling story, these goals keep the team focused and driven.",
    },
    {
      type: "textMessage",
      text: "It's like setting personal milestones to ensure they reach their destination.",
    },
  ],
  planArray: [
    {
      type: "textMessage",
      text: "Next comes Planning. This is where the team organizes the road ahead, deciding who will work on what and when.",
    },
    {
      type: "textMessage",
      text: "It's like planning a road trip; without a clear plan, they could get lost along the way, leading to chaos and delays.",
    },
  ],
  concArray: [
    {
      type: "textMessage",
      text: "With a plan in hand, the developers move to the Concept phase.",
    },
    {
      type: "textMessage",
      text: "This is like sketching the blueprint of a building, outlining the game's look, feel, and basic structure.",
    },
    {
      type: "textMessage",
      text: "Here, they create a shared vision that guides everyone involved, ensuring they stay on track.",
    },
  ],
};

let room_0_start = {
  lowerSrc: "./assets/rooms/0_room/bg.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 3, y: 3 }),
      isPlayerControlled: true,
      src: "./assets/player/player_basic.png",
      useShadow: false,
      useAnim: false,
      dontUseHalfY: true,
      speed: utils.character.speed,
      size: { width: 11, height: 11 },
      offset: { x: 10, y: 16 },
    }),
  },

  walls: walls_room_0_start,

  useMusic: false,
  cutsceneSpaces: {
    "1,1": [
      {
        type: "textMessage",
        text: "What are you looking for? Happiness? Haha",
      },
    ],

    // Inspiration
    "9,4": infoTexts_r0.inspoArray,
    "9,5": infoTexts_r0.inspoArray,

    // Goals
    "15,7": infoTexts_r0.goalsArray,
    "15,8": infoTexts_r0.goalsArray,

    // Planning
    "22,6": infoTexts_r0.planArray,
    "22,7": infoTexts_r0.planArray,

    // Concept
    "29,6": infoTexts_r0.concArray,
    "30,6": infoTexts_r0.concArray,
  },

  nextRooms: {
    "35,1": "hall_0",
  },

  resetIndex: 0,

  restartPositions: [{ x: 3, y: 3 }],
};
