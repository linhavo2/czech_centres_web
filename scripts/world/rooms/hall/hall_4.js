let events_h4 = {
  start: [
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "down", steps: 1 },
        { direction: "left", steps: 11 },
        { direction: "down", steps: 8 },
        { direction: "right", steps: 9 },
        { direction: "up", steps: 2 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
};

let hall_4 = {
  lowerSrc: "./assets/halls/hall_4/bg.png",
  upperSrc: "./assets/halls/hall_4/up.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 32, y: 11 }), // 13,4
      isPlayerControlled: true,
      src: "./assets/halls/hall_4/player.png",
      useShadow: true,
      size: { width: 11, height: 11 },
      offset: { x: 10, y: 16 },
      useSound: true,
      step: "./assets/audio/sounds/step1.mp3",
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 32, y: 13 }),
      src: "./assets/halls/hall_4/npc.png",
      useShadow: true,
      direction: "up",
      useAnim: true,
      size: { width: 12, height: 14 },
      offset: { x: 10, y: 14 },
      behaviorLoop: [],

      useSound: true,
      stepSound: "./assets/audio/sounds/step2.mp3",

      talking: [],
    }),
  },
  useMusic: true,

  walls: walls_hall_4,

  cutsceneSpaces: {
    "32,11": events_h4.start,
  },

  nextRooms: {
    "29,19": "room_5",
  },

  restartPositions: [{ x: 32, y: 11 }],
};
