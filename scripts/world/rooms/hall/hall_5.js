let events_h5 = {
  start: [
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 4 },
        { direction: "up", steps: 2 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
};

let hall_5 = {
  lowerSrc: "./assets/halls/hall_5/bg.png",
  upperSrc: "./assets/halls/hall_5/up.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 29, y: 20 }), // 13,4
      isPlayerControlled: true,
      src: "./assets/halls/hall_5/player.png",
      useShadow: true,
      useSound: true,
      size: { width: 11, height: 11 },
      offset: { x: 10, y: 16 },
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 29, y: 22 }),
      src: "./assets/halls/hall_5/npc.png",
      useShadow: true,
      useAnim: true,
      useSound: true,
      direction: "up",
      size: { width: 12, height: 14 },
      offset: { x: 10, y: 14 },
      behaviorLoop: [],

      talking: [],
    }),
  },

  useMusic: true,
  walls: walls_hall_5,

  cutsceneSpaces: {
    "29,20": events_h5.start,
  },

  nextRooms: {
    "32,19": "room_6",
  },

  restartPositions: [{ x: 29, y: 20 }],
};
