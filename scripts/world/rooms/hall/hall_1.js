const infoTexts_h1 = {
  introArray: [
    {
      type: "textMessage",
      who: "npc",
      text: "Alright, we have went through the Game design room. Visual room awaits us. I can't wait to look normal again.",
      count: 1,
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "down", steps: 1 },
        { direction: "right", steps: 13 },
        { direction: "up", steps: 2 },
      ],
      stopper: false,
      count: 1,
    },
  ],
};

let hall_1 = {
  lowerSrc: "./assets/halls/hall_1/bg.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 13, y: 3 }), // 13,4
      isPlayerControlled: true,
      src: "./assets/halls/hall_1/player.png",
      useAnim: false,
      dontUseHalfY: true,
      speed: utils.character.speed,
      size: { width: 11, height: 11 },
      offset: { x: 10, y: 16 },
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 13, y: 5 }),
      src: "./assets/npc/npc_basic.png",
      useShadow: false,
      useAnim: false,
      dontUseHalfY: true,
      speed: 1,
      size: { width: 11, height: 11 },
      offset: { x: 10, y: 14 },
      behaviorLoop: [],

      talking: [],
    }),
  },

  walls: walls_hall_1,

  nextRooms: {
    "25,3": "room_2",
  },

  cutsceneSpaces: {
    "13,4": infoTexts_h1.introArray,
  },

  restartPositions: [{ x: 13, y: 3 }],
};
