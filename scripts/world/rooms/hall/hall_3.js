let events_h3 = {
  start: [
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "left", steps: 7 },
        { direction: "down", steps: 7 },
        { direction: "right", steps: 12 },
        { direction: "up", steps: 2 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
};

let hall_3 = {
  lowerSrc: "./assets/halls/hall_3/bg.png",
  upperSrc: "./assets/halls/hall_3/up.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 28, y: 5 }), // 13,4
      isPlayerControlled: true,
      src: "./assets/halls/hall_3/player.png",
      useShadow: true,
      useSound: true,
      size: { width: 11, height: 11 },
      offset: { x: 10, y: 16 },
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 28, y: 7 }),
      src: "./assets/halls/hall_3/npc.png",
      useShadow: true,
      useAnim: true,
      useSound: true,
      direction: "up",
      size: { width: 12, height: 14 },
      offset: { x: 10, y: 14 },
      behaviorLoop: [],

      talking: [],
    }),
  },

  useMusic: true,

  walls: walls_hall_3,

  cutsceneSpaces: {
    "28,5": events_h3.start,
  },

  nextRooms: {
    "32,11": "room_4",
  },

  restartPositions: [{ x: 28, y: 5 }],
};
