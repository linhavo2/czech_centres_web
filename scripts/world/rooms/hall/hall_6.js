let events_h6 = {
  start: [
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "left", steps: 12 },
        { direction: "up", steps: 7 },
        { direction: "left", steps: 11 },
        { direction: "down", steps: 3 },
        { direction: "left", steps: 7 },
        { direction: "up", steps: 2 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
};

let hall_6 = {
  lowerSrc: "./assets/halls/hall_6/bg.png",
  upperSrc: "./assets/halls/hall_6/up.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 32, y: 20 }),
      isPlayerControlled: true,
      src: "./assets/halls/hall_6/player.png",
      useShadow: true,
      useSound: true,
      size: { width: 11, height: 11 },
      offset: { x: 10, y: 16 },
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 32, y: 22 }),
      src: "./assets/halls/hall_6/npc.png",
      useShadow: true,
      useSound: true,
      useAnim: true,
      direction: "up",
      size: { width: 12, height: 14 },
      offset: { x: 10, y: 14 },
      behaviorLoop: [],

      talking: [],
    }),
  },
  useMusic: true,

  walls: walls_hall_6,

  cutsceneSpaces: {
    "32,20": events_h6.start,
  },

  nextRooms: {
    "3,15": "room_7",
  },

  restartPositions: [{ x: 32, y: 20 }],
};
