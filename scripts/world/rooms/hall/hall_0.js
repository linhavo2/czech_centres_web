const infoTexts_h0 = {
  introArray: [
    {
      type: "textMessage",
      text: "This room is hall. It is a place where you can find doors to other rooms. And to which you will return after visiting them.",
    },
    {
      type: "textMessage",
      text: "This is also a place where you will see improvements from each room. So, keep an eye on them!",
    },
    {
      type: "textMessage",
      text: "You can walk through the doors to enter the rooms. But remember, you can't walk through the walls! Or do you?",
    },
  ],
};

let hall_0 = {
  lowerSrc: "./assets/halls/hall_0/bg.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 2, y: 2 }),
      isPlayerControlled: true,

      src: "./assets/halls/hall_0/player.png",
      useShadow: false,
      useAnim: false,
      dontUseHalfY: true,
      speed: utils.character.speed,
      size: { width: 11, height: 11 },
      offset: { x: 10, y: 16 },
    }),
  },

  walls: walls_hall_0,

  nextRooms: {
    "13,3": "room_1",
  },

  cutsceneSpaces: {
    "6,5": infoTexts_h0.introArray,
    "6,6": infoTexts_h0.introArray,
    "6,7": infoTexts_h0.introArray,
  },

  restartPositions: [{ x: 2, y: 2 }],
};
