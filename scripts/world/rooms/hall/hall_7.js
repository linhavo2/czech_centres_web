let hall_7 = {
  lowerSrc: "./assets/halls/hall_7/bg.png",
  upperSrc: "./assets/halls/hall_7/up.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 29, y: 20 }), // 13,4
      isPlayerControlled: true,
      src: "./assets/halls/hall_7/player.png",
      useSound: true,
      useShadow: true,
      size: { width: 11, height: 11 },
      offset: { x: 10, y: 16 },
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 16, y: 11 }),
      src: "./assets/halls/hall_7/npc.png",
      useShadow: true,
      useSound: true,
      useAnim: true,
      size: { width: 12, height: 14 },
      offset: { x: 10, y: 14 },
      behaviorLoop: [],

      talking: [],
    }),
  },
  useMusic: true,

  walls: walls_hall_7,

  nextRooms: {
    "32,19": "room_7",
  },

  restartPositions: [{ x: 29, y: 20 }],
};
