let events_h2 = {
  start: [
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 4 },
        { direction: "up", steps: 2 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
};

let hall_2 = {
  lowerSrc: "./assets/halls/hall_2/bg.png",
  upperSrc: "./assets/halls/hall_2/up.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 25, y: 5 }),
      isPlayerControlled: true,
      src: "./assets/halls/hall_2/player.png",
      useShadow: true,
      size: { width: 11, height: 11 },
      offset: { x: 10, y: 16 },
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 25, y: 7 }),
      src: "./assets/halls/hall_2/npc.png",
      useShadow: true,
      direction: "up",
      useAnim: true,
      size: { width: 12, height: 14 },
      offset: { x: 10, y: 14 },
      behaviorLoop: [],

      talking: [],
    }),
  },

  walls: walls_hall_2,

  cutsceneSpaces: {
    "25,5": events_h2.start,
  },

  nextRooms: {
    "28,4": "room_3",
  },

  restartPositions: [{ x: 25, y: 5 }],
};
