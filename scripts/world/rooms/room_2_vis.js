const events_r2 = {
  concArray: [
    {
      type: "textMessage",
      text: "With the game design outlined, the team shifts focus to Visuals.",
    },
    {
      type: "textMessage",
      text: "This journey starts with Concept Art, where artists sketch the initial ideas for characters, landscapes, and objects.",
    },
    {
      type: "textMessage",
      text: "These early drawings help establish the game's visual style, ensuring it looks cohesive and attractive.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "up", steps: 3 },
        { direction: "right", steps: 8 },
        { direction: "up", steps: 4 },
        { direction: "right", steps: 5 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
  spriteArray: [
    {
      type: "textMessage",
      who: "player",
      text: "Now, we will finally talk about updating visuals of the game. Starting with you.",
      count: 1,
    },
    {
      type: "switchSprite",
      what: "player",
      sprite: {
        src: "./assets/player/player.png",
        useShadow: true,
        useAnim: false,
      },
      count: 1,
    },
    {
      type: "textMessage",
      text: "What do you think? Do you like your new style? Now for me.",
      count: 1,
    },
    {
      type: "switchSprite",
      what: "npc",
      sprite: {
        src: "./assets/npc/npc.png",
        useShadow: true,
        useAnim: false,
      },
      count: 1,
    },
    {
      type: "textMessage",
      who: "player",
      text: "Did you expect this? Haha that is what I thought. Now the background. It is a bit boring like this, right?",
      count: 1,
    },
    {
      type: "switchSprite",
      what: "map",
      where: "lower",
      src: "./assets/rooms/2_room/room_2_bg_alt.png",
      count: 1,
    },
    {
      type: "switchSprite",
      what: "map",
      where: "upper",
      src: "./assets/rooms/2_room/room_2_up_alt.png",
      count: 1,
    },
    {
      type: "textMessage",
      who: "player",
      text: "Much better! Now the game looks more interesting.",
      count: 1,
    },
    {
      type: "textMessage",
      text: "Using Sprites is important to make the game more appealing to the player. Let's move to the next one.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 4 },
        { direction: "down", steps: 9 },
        { direction: "right", steps: 5 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
  animArray: [
    {
      type: "textMessage",
      text: "But it's Animation that truly brings the game to life.",
    },
    {
      type: "switchSprite",
      what: "player",
      sprite: {
        src: "./assets/player/player.png",
        useShadow: true,
        useAnim: true,
      },
    },
    {
      type: "textMessage",
      text: "By adding movement and action to characters and objects, animation creates a dynamic and immersive experience.",
    },
    {
      type: "textMessage",
      text: "It's like breathing life into a static picture, making the game world feel vibrant and alive. How do you feel? Now for me.",
    },
    {
      type: "switchSprite",
      what: "npc",
      sprite: {
        src: "./assets/npc/npc.png",
        useShadow: true,
        useAnim: true,
      },
    },

    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 5 },
        { direction: "up", steps: 7 },
        { direction: "right", steps: 5 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
  UiUxArray: [
    {
      type: "textMessage",
      text: "To ensure players can navigate easily, the team designs the UI/UX.",
    },
    {
      type: "textMessage",
      text: "This includes the menus, buttons, and other interface elements that guide players through the game.",
    },
    {
      type: "textMessage",
      text: "A well-designed UI/UX ensures players can focus on the fun without getting lost in complex controls.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 9 },
        { direction: "up", steps: 1 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
};

let room_2_vis = {
  lowerSrc: "./assets/rooms/2_room/room_2_bg.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 2, y: 16 }),
      isPlayerControlled: true,
      src: "./assets/player/player_basic.png",
      useShadow: false,
      useAnim: false,
      dontUseHalfY: true,
      size: { width: 11, height: 11 },
      offset: { x: 10, y: 16 },
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 2, y: 12 }),
      src: "./assets/npc/npc_basic.png",
      useShadow: false,
      useAnim: false,
      dontUseHalfY: true,
      size: { width: 11, height: 11 },
      offset: { x: 10, y: 14 },
      behaviorLoop: [],
    }),
  },

  nextRooms: {
    "42,5": "hall_2",
  },

  walls: walls_room_2_vis,

  useMusic: false,
  cutsceneSpaces: {
    // Concept Art
    "1,14": events_r2.concArray,
    "2,14": events_r2.concArray,

    // Sprites
    "12,5": events_r2.spriteArray,
    "12,6": events_r2.spriteArray,

    // Animation
    "22,13": events_r2.animArray,
    "22,14": events_r2.animArray,

    // UI/UX
    "32,7": events_r2.UiUxArray,
    "32,8": events_r2.UiUxArray,
  },

  restartPositions: [{ x: 2, y: 16 }],
};
