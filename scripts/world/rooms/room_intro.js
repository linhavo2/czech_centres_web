const events_intro = {};

let room_intro = {
  lowerSrc: "./assets/rooms/intro_room/bg.png",
  upperSrc: "./assets/rooms/intro_room/up.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 31, y: 22 }),
      direction: "right",
      isPlayerControlled: true,
      src: "./assets/player/player.png",
      useShadow: true,
      speed: utils.character.speed,
      size: utils.character.size,
      offset: utils.character.offset,
      useSound: true,
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 35, y: 22 }),
      src: "./assets/rooms/intro_room/npc.png",
      useShadow: true,
      speed: 1,
      direction: "left",
      useSound: true,
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
  },
  useMusic: true,

  cutsceneSpaces: {},

  walls: walls_room_intro,

  nextRooms: {
    "33,18": "room_0",
  },
};
