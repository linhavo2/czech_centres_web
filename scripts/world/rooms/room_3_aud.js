const events_r3 = {
  musicArray: [
    {
      type: "textMessage",
      who: "npc",
      text: "The audio journey begins with Music, setting the mood and atmosphere for the game.",
    },
    { type: "useMusic" },
    {
      type: "textMessage",
      text: "Like a movie soundtrack, the right music can evoke emotions and create a memorable experience.",
    },
    {
      type: "textMessage",
      text: "Whether it's an epic battle theme or a calming background tune, music adds depth to the game.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 4 },
        { direction: "down", steps: 5 },
        { direction: "right", steps: 7 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
  soundArray: [
    {
      type: "textMessage",
      text: "Sound Effects are the noises that bring the game world to life.",
    },
    {
      type: "textMessage",
      text: "From the crunch of footsteps to the roar of explosions, these sounds make the environment feel real and engaging.",
    },
    {
      type: "textMessage",
      text: "Properly crafted sound effects enhance immersion, making players feel like they're part of the action.",
    },
    { type: "useSound", who: "player" },
    { type: "useSound", who: "npc" },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 3 },
        { direction: "up", steps: 7 },
        { direction: "right", steps: 5 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
  voiceArray: [
    { type: "voice", src: "./assets/audio/voice/voice1.mp3" },
    {
      type: "textMessage",
      text: "Voice Acting adds personality and charm to the game's characters. Voice actors give them unique voices, helping players connect with their journey and story.",
    },
    { type: "voice", src: "./assets/audio/voice/voice2.mp3" },
    {
      type: "textMessage",
      text: "It's like having a great narrator in a book, guiding you through the adventure.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 4 },
        { direction: "down", steps: 5 },
        { direction: "right", steps: 8 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
  ambientArray: [
    {
      type: "textMessage",
      text: "Finally, Ambient Sounds create the backdrop for the game, providing subtle cues that make the world feel authentic.",
    },
    {
      type: "textMessage",
      text: "Birds chirping, wind rustling, and distant city noise all contribute to a realistic environment. These sounds add an extra layer of immersion, making the game world feel lived in.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 4 },
        { direction: "up", steps: 3 },
        { direction: "right", steps: 9 },
        { direction: "up", steps: 2 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
};

let room_3_aud = {
  lowerSrc: "./assets/rooms/3_room/room_3_bg.png",
  upperSrc: "./assets/rooms/3_room/room_3_up.png",
  useMusic: false,
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 2, y: 8 }),
      direction: "up",
      isPlayerControlled: true,
      src: "./assets/player/player.png",
      useShadow: true,
      speed: utils.character.speed,
      size: utils.character.size,
      offset: utils.character.offset,
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 6, y: 6 }),
      src: "./assets/npc/npc.png",
      useShadow: true,
      direction: "left",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
  },

  cutsceneSpaces: {
    // Music
    "4,6": events_r3.musicArray,
    "4,7": events_r3.musicArray,

    // Sound Effects
    "15,10": events_r3.soundArray,
    "15,11": events_r3.soundArray,

    // Voice Acting
    "23,3": events_r3.voiceArray,
    "23,4": events_r3.voiceArray,

    // Ambient Sounds
    "35,8": events_r3.ambientArray,
    "35,9": events_r3.ambientArray,
  },

  walls: walls_room_3_aud,

  nextRooms: {
    "49,3": "hall_3",
  },

  restartPositions: [{ x: 2, y: 8 }],
};
