const events_r5 = {
  qualArray: [
    {
      type: "textMessage",
      text: "As the game development journey nears completion, it's time for Testing.",
    },
    {
      type: "textMessage",
      text: "The first step is Quality Assurance (QA), where testers play the game to find bugs and issues.",
    },
    {
      type: "textMessage",
      text: "They're like detectives, uncovering hidden problems and ensuring the game is polished before release.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 4 },
        { direction: "up", steps: 8 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  expArray: [
    {
      type: "textMessage",
      text: "User Experience Testing focuses on gathering feedback from players to understand their experience.",
    },
    {
      type: "textMessage",
      text: "This helps identify what's working well and what needs improvement.",
    },
    {
      type: "textMessage",
      text: "It's a key step in making sure the game is enjoyable and intuitive for players.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 9 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  perfArray: [
    {
      type: "textMessage",
      text: "Performance Testing is about checking how the game runs on different devices and platforms.",
    },
    {
      type: "textMessage",
      text: "Testers ensure the game operates smoothly without lag or crashes, regardless of the hardware.",
    },
    {
      type: "textMessage",
      text: "This is crucial for delivering a consistent experience across a wide range of devices.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 3 },
        { direction: "down", steps: 9 },
        { direction: "right", steps: 5 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  compArray: [
    {
      type: "textMessage",
      text: "Compatibility Testing ensures the game works across different operating systems and environments.",
    },
    {
      type: "textMessage",
      text: "This step is essential to reach the widest audience, ensuring the game is playable on various platforms.",
    },
    {
      type: "textMessage",
      text: "Without compatibility testing, some players might be unable to enjoy the game.",
    },
    {
      type: "textMessage",
      text: "To know that everything is working as intended, the game must be walked through on different devices.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "up", steps: 1 },
        { direction: "right", steps: 2 },
        { direction: "up", steps: 3 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
};

let room_5_test = {
  lowerSrc: "./assets/rooms/5_room/bg_alt.png",
  upperSrc: "./assets/rooms/5_room/up_alt.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 2, y: 20 }),
      direction: "up",
      isPlayerControlled: true,
      src: "./assets/player/player.png",
      useShadow: true,
      speed: utils.character.speed,
      size: utils.character.size,
      offset: utils.character.offset,
      useSound: true,
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 2, y: 15 }),
      src: "./assets/npc/npc.png",
      useShadow: true,
      speed: 1,
      useSound: true,
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    key_1: new Collectible({
      position: { x: 375, y: 216 },
      src: "./assets/obstacles/collectibles/key_1.png",
      size: {
        width: 15,
        height: 9,
      },
      offset: {
        x: 9,
        y: 9,
      },
    }),
    key_2: new Collectible({
      position: { x: 424, y: 233 },
      src: "./assets/obstacles/collectibles/key_2.png",
      size: {
        width: 9,
        height: 9,
      },
      offset: {
        x: 12,
        y: 13,
      },
    }),
    key_3: new Collectible({
      position: { x: 397, y: 270 },
      src: "./assets/obstacles/collectibles/key_3.png",
      size: {
        width: 13,
        height: 17,
      },
      offset: {
        x: 11,
        y: 14,
      },
    }),
  },
  useMusic: true,

  cutsceneSpaces: {
    "1,17": events_r5.qualArray,
    "2,17": events_r5.qualArray,

    "5,9": events_r5.expArray,
    "6,9": events_r5.expArray,

    "13,6": events_r5.perfArray,
    "13,7": events_r5.perfArray,

    "21,16": events_r5.compArray,
    "21,17": events_r5.compArray,
  },

  walls: walls_room_5_test,

  collectibles: ["key_1", "key_2", "key_3"],

  nextRooms: {
    "33,10": "hall_5",
  },

  restartPositions: [{ x: 2, y: 20 }],
};
