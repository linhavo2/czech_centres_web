const events_r4 = {
  engineArray: [
    {
      type: "textMessage",
      text: "The programming journey begins with Engine Selection, where developers choose the technology they'll use to build the game.",
    },
    {
      type: "textMessage",
      text: "This decision is crucial, as it determines the game's capabilities and technical framework.",
    },
    {
      type: "textMessage",
      text: "It's like picking the right tools for construction; the wrong choice could limit the game's potential.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 8 },
        { direction: "down", steps: 3 },
        { direction: "right", steps: 6 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  archArray: [
    {
      type: "textMessage",
      text: "With the engine selected, they focus on Code Architecture, the structure and organization of the codebase.",
    },
    {
      type: "textMessage",
      text: "This is the backbone of the game, ensuring the code is clean and manageable.",
    },
    {
      type: "textMessage",
      text: "A well-architected codebase makes development smoother and reduces the chances of bugs and crashes.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 2 },
        { direction: "up", steps: 6 },
        { direction: "right", steps: 5 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  featureArray: [
    {
      type: "textMessage",
      text: "Feature Implementation is where programmers write the code that makes the game work.",
    },
    {
      type: "textMessage",
      text: "This involves everything from character movement to complex mechanics, turning ideas into reality.",
    },
    {
      type: "textMessage",
      text: "Proper implementation ensures the game functions smoothly and delivers a seamless experience to players.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 4 },
        { direction: "down", steps: 7 },
        { direction: "right", steps: 4 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  optArray: [
    {
      type: "restartIndex",
    },
    {
      type: "textMessage",
      text: "To keep the game running efficiently, programmers work on Optimization.",
    },
    {
      type: "textMessage",
      text: "This involves fine-tuning the code to reduce lag and improve performance.",
    },
    {
      type: "textMessage",
      text: "It's like tuning an engine to get the best performance; without optimization, the game could slow down and frustrate players.",
    },
    {
      type: "teleport",
      who: "npc",
      position: utils.gridToPos({ x: 37, y: 9 }),
      direction: "right",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 4 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  docArray: [
    {
      type: "textMessage",
      text: "Documentation is the final step, where programmers create guides and manuals that explain how the code works.",
    },
    {
      type: "textMessage",
      text: "This is crucial for helping other developers understand the project and for maintaining it in the future.",
    },
    {
      type: "textMessage",
      text: "Good documentation ensures the game's longevity and eases the handover to new team members.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 9 },
        { direction: "up", steps: 2 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
};

let room_4_progr = {
  lowerSrc: "./assets/rooms/4_room/bg.png",
  upperSrc: "./assets/rooms/4_room/up.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 2, y: 12 }),
      direction: "up",
      isPlayerControlled: true,
      src: "./assets/player/player.png",
      useShadow: true,
      speed: utils.character.speed,
      size: utils.character.size,
      offset: utils.character.offset,
      useSound: true,
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 2, y: 8 }),
      src: "./assets/npc/npc.png",
      useShadow: true,
      speed: 1,
      useSound: true,
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),

    laser1: new Laser({
      position: { x: 525, y: 131 },
      src: "./assets/obstacles/laser/1.png",
      active: true,
    }),

    laser2: new Laser({
      position: { x: 565, y: 131 },
      src: "./assets/obstacles/laser/1.png",
    }),
  },
  useMusic: true,

  cutsceneSpaces: {
    // Programming
    "1,10": events_r4.engineArray,
    "2,10": events_r4.engineArray,

    // Architecture
    "13,11": events_r4.archArray,
    "13,12": events_r4.archArray,

    // Features
    "21,4": events_r4.featureArray,
    "21,5": events_r4.featureArray,

    // Optimization
    "30,12": events_r4.optArray,
    "30,13": events_r4.optArray,

    // Documentation
    "39,9": events_r4.docArray,
    "39,10": events_r4.docArray,
  },

  walls: walls_room_4_progr,

  nextRooms: {
    "49,6": "hall_4",
  },

  restartPositions: [
    { x: 2, y: 12 },
    { x: 31, y: 12 },
  ],
};
