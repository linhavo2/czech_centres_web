const events_r6 = {
  reseArray: [
    {
      type: "textMessage",
      text: "With the game development nearly complete, the focus shifts to Marketing.",
    },
    {
      type: "textMessage",
      text: "The first step is Market Research, where marketers explore what players want in a game.",
    },
    {
      type: "textMessage",
      text: "By understanding current trends and preferences, they can tailor the marketing strategy to attract more players.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 3 },
        { direction: "up", steps: 7 },
        { direction: "right", steps: 4 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  brandArray: [
    {
      type: "textMessage",
      text: "Branding is about creating a unique identity for the game, including logos, color schemes, and visual designs.",
    },
    {
      type: "textMessage",
      text: "A strong brand makes the game recognizable and memorable, helping it stand out in a crowded market.",
    },
    {
      type: "textMessage",
      text: "Effective branding can generate buzz and excitement before the game's release.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 2 },
        { direction: "up", steps: 3 },
        { direction: "right", steps: 6 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  promoArray: [
    {
      type: "textMessage",
      text: "Promotional Materials are the ads, trailers, and posters used to showcase the game to the public.",
    },
    {
      type: "textMessage",
      text: "These materials serve as teasers, creating anticipation and drawing interest.",
    },
    {
      type: "textMessage",
      text: "A compelling promotional campaign can boost pre-orders and build a loyal fan base before launch.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 2 },
        { direction: "down", steps: 1 },
        { direction: "right", steps: 5 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  commArray: [
    {
      type: "textMessage",
      text: "Community Engagement involves interacting with players and building a community around the game.",
    },
    {
      type: "textMessage",
      text: "This can happen online through social media or in person at gaming events.",
    },
    {
      type: "textMessage",
      text: "Engaging with the community creates a sense of belonging and loyalty, fostering a vibrant player base that supports the game over time.",
    },
    {
      type: "textMessage",
      text: "To find a way, in a world full of games and tastes, into hearts of players is not easy. But it is very important.",
    },
    {
      type: "teleport",
      who: "npc",
      position: utils.gridToPos({ x: 32, y: 10 }),
      direction: "right",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 4 },
        { direction: "up", steps: 2 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
};

let room_6_mark = {
  lowerSrc: "./assets/rooms/6_room/bg.png",
  upperSrc: "./assets/rooms/6_room/up.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 2, y: 17 }),
      direction: "up",
      isPlayerControlled: true,
      src: "./assets/player/player.png",
      useShadow: true,
      useSound: true,
      speed: utils.character.speed,
      size: utils.character.size,
      offset: utils.character.offset,
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 5, y: 15 }),
      src: "./assets/npc/npc.png",
      useShadow: true,
      direction: "left",
      useSound: true,
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    red3: new Person({
      position: utils.gridToPos({ x: 28, y: 7 }),
      src: "./assets/obstacles/people/red/3.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
      talking: [utils.getRandomTalk()],
    }),
    red4: new Person({
      position: utils.gridToPos({ x: 30, y: 6 }),
      src: "./assets/obstacles/people/red/4.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
      talking: [utils.getRandomTalk()],
    }),
    red5: new Person({
      position: utils.gridToPos({ x: 27, y: 8 }),
      src: "./assets/obstacles/people/red/5.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
      talking: [utils.getRandomTalk()],
    }),
    red6: new Person({
      position: utils.gridToPos({ x: 30, y: 8 }),
      src: "./assets/obstacles/people/red/6.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
      talking: [utils.getRandomTalk()],
    }),
    red7: new Person({
      position: utils.gridToPos({ x: 31, y: 8 }),
      src: "./assets/obstacles/people/red/7.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
      talking: [utils.getRandomTalk()],
    }),
    red8: new Person({
      position: utils.gridToPos({ x: 27, y: 9 }),
      src: "./assets/obstacles/people/red/2.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
      talking: [utils.getRandomTalk()],
    }),
    red9: new Person({
      position: utils.gridToPos({ x: 27, y: 10 }),
      src: "./assets/obstacles/people/red/3.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
      talking: [utils.getRandomTalk()],
    }),
    red10: new Person({
      position: utils.gridToPos({ x: 29, y: 9 }),
      src: "./assets/obstacles/people/red/4.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
      talking: [utils.getRandomTalk()],
    }),
    red11: new Person({
      position: utils.gridToPos({ x: 31, y: 10 }),
      src: "./assets/obstacles/people/red/2.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
      talking: [utils.getRandomTalk()],
    }),

    blue2: new Person({
      position: utils.gridToPos({ x: 27, y: 6 }),
      src: "./assets/obstacles/people/blue/5.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue3: new Person({
      position: utils.gridToPos({ x: 28, y: 6 }),
      src: "./assets/obstacles/people/blue/6.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue4: new Person({
      position: utils.gridToPos({ x: 29, y: 6 }),
      src: "./assets/obstacles/people/blue/7.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue5: new Person({
      position: utils.gridToPos({ x: 31, y: 6 }),
      src: "./assets/obstacles/people/blue/5.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue6: new Person({
      position: utils.gridToPos({ x: 29, y: 7 }),
      src: "./assets/obstacles/people/blue/2.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue7: new Person({
      position: utils.gridToPos({ x: 30, y: 7 }),
      src: "./assets/obstacles/people/blue/3.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue8: new Person({
      position: utils.gridToPos({ x: 31, y: 7 }),
      src: "./assets/obstacles/people/blue/4.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue9: new Person({
      position: utils.gridToPos({ x: 28, y: 8 }),
      src: "./assets/obstacles/people/blue/6.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue10: new Person({
      position: utils.gridToPos({ x: 29, y: 8 }),
      src: "./assets/obstacles/people/blue/7.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue11: new Person({
      position: utils.gridToPos({ x: 28, y: 9 }),
      src: "./assets/obstacles/people/blue/5.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue12: new Person({
      position: utils.gridToPos({ x: 90, y: 9 }),
      src: "./assets/obstacles/people/blue/3.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue13: new Person({
      position: utils.gridToPos({ x: 31, y: 9 }),
      src: "./assets/obstacles/people/blue/4.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue14: new Person({
      position: utils.gridToPos({ x: 28, y: 10 }),
      src: "./assets/obstacles/people/blue/2.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue15: new Person({
      position: utils.gridToPos({ x: 29, y: 10 }),
      src: "./assets/obstacles/people/blue/6.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue16: new Person({
      position: utils.gridToPos({ x: 30, y: 10 }),
      src: "./assets/obstacles/people/blue/7.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue17: new Person({
      position: utils.gridToPos({ x: 30, y: 9 }),
      src: "./assets/obstacles/people/blue/2.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
    blue18: new Person({
      position: utils.gridToPos({ x: 27, y: 7 }),
      src: "./assets/obstacles/people/blue/4.png",
      useShadow: true,
      direction: "down",
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
  },

  useMusic: true,

  cutsceneSpaces: {
    // Research
    "3,14": events_r6.reseArray,
    "3,15": events_r6.reseArray,

    // Branding
    "10,7": events_r6.brandArray,
    "10,8": events_r6.brandArray,

    // Promotional
    "18,4": events_r6.promoArray,
    "18,5": events_r6.promoArray,

    // Community
    "26,6": events_r6.commArray,
    "26,7": events_r6.commArray,
  },

  walls: walls_room_6_mark,

  nextRooms: {
    "35,7": "hall_6",
  },

  restartPositions: [{ x: 2, y: 17 }],
};
