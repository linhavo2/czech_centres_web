const events_r8 = {
  reflArray: [
    {
      type: "textMessage",
      text: "Congratulations! You have completed the game development journey and released your game to the world.",
    },
    {
      type: "textMessage",
      text: "As the journey comes to an end, developers enter the End phase, where they reflect on their achievements and plan for the future.",
    },
    {
      type: "textMessage",
      text: "Reflection is the first step, allowing them to look back at the development process and understand what worked and what could be improved.",
    },
    {
      type: "textMessage",
      text: "This reflection helps them grow as developers and make better games in the future.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "up", steps: 3 },
        { direction: "left", steps: 8 },
        { direction: "right", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  feedArray: [
    {
      type: "textMessage",
      text: "Player Feedback is about listening to the players, gathering insights into what they enjoyed and what they found lacking.",
    },
    {
      type: "textMessage",
      text: "This feedback is invaluable for understanding the player's perspective and guiding future development.",
    },
    {
      type: "textMessage",
      text: "It helps developers create games that resonate with their audience.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "left", steps: 3 },
        { direction: "down", steps: 6 },
        { direction: "left", steps: 5 },
        { direction: "right", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  postArray: [
    {
      type: "textMessage",
      text: "A Post-Mortem is the final analysis, where developers document the entire game development process.",
    },
    {
      type: "textMessage",
      text: "This step involves writing down lessons learned, successes, and areas for improvement.",
    },
    {
      type: "textMessage",
      text: "A thorough post-mortem helps developers refine their approach and avoid repeating mistakes in future projects.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "left", steps: 3 },
        { direction: "up", steps: 9 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  celeArray: [
    {
      type: "textMessage",
      text: "After all the hard work, it's time for a Celebration.",
    },
    {
      type: "textMessage",
      text: "This is when developers acknowledge their achievements and celebrate the success of their project.",
    },
    {
      type: "textMessage",
      text: "It's like a grand finale, a chance to relax and enjoy the fruits of their labor.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 2 },
        { direction: "up", steps: 8 },
        { direction: "right", steps: 13 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  futurArray: [
    {
      type: "textMessage",
      text: "Finally, developers consider their Future Plans.",
    },
    {
      type: "textMessage",
      text: "This is where they brainstorm ideas for the next game, whether it's a sequel or something entirely new.",
    },
    {
      type: "textMessage",
      text: "Future plans mark the beginning of another exciting journey in game development, filled with new ideas and endless possibilities.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 4 },
        { direction: "down", steps: 2 },
        { direction: "right", steps: 6 },
        { direction: "up", steps: 1 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
};

let room_8_end = {
  lowerSrc: "./assets/rooms/8_room/bg.png",
  upperSrc: "./assets/rooms/8_room/up.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 24, y: 24 }),
      direction: "up",
      isPlayerControlled: true,
      src: "./assets/player/player.png",
      useShadow: true,
      speed: utils.character.speed,
      size: utils.character.size,
      offset: utils.character.offset,
      useSound: true,
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 24, y: 19 }),
      src: "./assets/npc/npc.png",
      useShadow: true,
      speed: 1,
      direction: "down",
      useSound: true,
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
  },
  useMusic: true,

  cutsceneSpaces: {
    // Reflection
    "24,21": events_r8.reflArray,
    "25,21": events_r8.reflArray,

    // Feedback
    "18,15": events_r8.feedArray,
    "18,16": events_r8.feedArray,

    // Post-Mortem
    "10,22": events_r8.postArray,
    "10,23": events_r8.postArray,

    // Celebration
    "4,15": events_r8.celeArray,
    "5,15": events_r8.celeArray,

    // Future Plans
    "18,4": events_r8.futurArray,
    "18,5": events_r8.futurArray,
  },

  walls: walls_room_8_end,

  nextRooms: {
    "29,5": "end",
  },

  restartPositions: [{ x: 24, y: 24 }],
};
