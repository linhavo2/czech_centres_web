const events_r7 = {
  platfArray: [
    {
      type: "textMessage",
      text: "Finally, the journey leads to Publishing, where the game is released to the world.",
    },
    {
      type: "textMessage",
      text: "The first step is Platform Selection, deciding whether the game will be available on computers, consoles, or mobile devices.",
    },
    {
      type: "textMessage",
      text: "This decision affects the game's distribution and potential audience.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "up", steps: 2 },
        { direction: "left", steps: 9 },
        { direction: "right", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  distrArray: [
    {
      type: "textMessage",
      text: "Distribution is the process of sending the game to stores or online platforms for purchase and download.",
    },
    {
      type: "textMessage",
      text: "It's the step that makes the game accessible to players, allowing them to enjoy the final product.",
    },
    {
      type: "textMessage",
      text: "Proper distribution is key to reaching a wide audience and generating sales.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "left", steps: 2 },
        { direction: "up", steps: 4 },
        { direction: "left", steps: 5 },
        { direction: "right", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  releArray: [
    {
      type: "textMessage",
      text: "Release Planning is where developers set the game's launch date and plan related events or promotions.",
    },
    {
      type: "textMessage",
      text: "It's like preparing for a big premiere; proper planning ensures a successful and smooth launch.",
    },
    {
      type: "textMessage",
      text: "Effective release planning can create excitement and drive sales.",
    },
    {
      type: "textMessage",
      text: "Producers are pushing whole team to their limits to make sure the game is ready for the release date.",
    },
    {
      type: "textMessage",
      text: "Can you meet the deadline?",
    },
    {
      type: "walk",
      who: "npc",
      direction: "left",
      steps: 1,
      count: 1,
    },
    {
      type: "teleport",
      who: "npc",
      position: utils.gridToPos({ x: 9, y: 8 }),
      direction: "right",
    },
  ],

  postArray: [
    {
      type: "textMessage",
      text: "Now you have your game released, but the work is not done yet.",
    },
    {
      type: "textMessage",
      text: "Post-Launch Support is the ongoing work after the game's release.",
    },
    {
      type: "textMessage",
      text: "This involves fixing bugs, adding new features, and addressing player feedback.",
    },
    {
      type: "textMessage",
      text: "Post-launch support is crucial for keeping players engaged and extending the game's lifespan.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "left", steps: 2 },
        { direction: "down", steps: 7 },
        { direction: "left", steps: 6 },
        { direction: "up", steps: 2 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],

  producerArray: [
    { type: "restartIndex" },
    {
      type: "spawn",
      who: "producer",
      position: utils.gridToPos({ x: 30, y: 8 }),
      direction: "left",
    },
    {
      type: "walk",
      who: "producer",
      direction: "left",
      steps: 3,
      stopper: false,
    },
    {
      type: "walk",
      who: "producer",
      direction: "up",
      steps: 5,
      stopper: false,
    },
    {
      type: "walk",
      who: "producer",
      direction: "left",
      steps: 3,
      stopper: false,
    },
    {
      type: "walk",
      who: "producer",
      direction: "down",
      steps: 3,
      stopper: false,
    },
    {
      type: "walk",
      who: "producer",
      direction: "left",
      steps: 3,
      stopper: false,
    },
    {
      type: "walk",
      who: "producer",
      direction: "down",
      steps: 3,
      stopper: false,
    },
    {
      type: "walk",
      who: "producer",
      direction: "left",
      steps: 3,
      stopper: false,
    },
    {
      type: "walk",
      who: "producer",
      direction: "up",
      steps: 5,
      stopper: false,
    },
    {
      type: "walk",
      who: "producer",
      direction: "left",
      steps: 3,
      stopper: false,
    },
  ],
};

let room_7_publ = {
  lowerSrc: "./assets/rooms/7_room/bg.png",
  upperSrc: "./assets/rooms/7_room/up.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 46, y: 18 }),
      direction: "up",
      isPlayerControlled: true,
      src: "./assets/player/player.png",
      useShadow: true,
      speed: utils.character.speed,
      size: utils.character.size,
      offset: utils.character.offset,
      useSound: true,
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 46, y: 14 }),
      src: "./assets/npc/npc.png",
      useShadow: true,
      speed: 1,
      direction: "down",
      useSound: true,
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),

    producer: new Person({
      position: utils.gridToPos({ x: 0, y: 0 }),
      src: "./assets/obstacles/producer/sprite.png",
      useShadow: true,
      visible: false,
      speed: 1,
      direction: "down",
      useSound: true,
      size: {
        width: 10,
        height: 12,
      },
      offset: {
        x: 9,
        y: 12,
      },
    }),
  },

  useMusic: true,

  cutsceneSpaces: {
    // Platform Selection
    "46,16": events_r7.platfArray,
    "47,16": events_r7.platfArray,

    // Distribution
    "39,12": events_r7.distrArray,
    "39,11": events_r7.distrArray,

    // Release Planning
    "31,8": events_r7.releArray,

    // Post-Launch Support
    "11,8": events_r7.postArray,
    "11,9": events_r7.postArray,

    // Producer
    "27,8": events_r7.producerArray,
    // "14,4": [{ type: "restartIndex" }],
  },

  walls: walls_room_7_publ,

  nextRooms: { "2,12": "room_8" },

  restartPositions: [
    { x: 46, y: 18 },
    { x: 30, y: 8 },
    // { x: 14, y: 4 },
  ],
};
