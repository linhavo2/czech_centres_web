const infoTexts_r1 = {
  mechArray: [
    {
      type: "textMessage",
      who: "npc",
      text: "Now the team dives into Game Design, starting with Game Mechanics. These are the rules and systems that shape how the game is played.",
    },
    {
      type: "textMessage",
      text: "They define what players can do and how they win, ensuring the game is balanced and fun.",
    },
  ],
  charArray: [
    {
      type: "textMessage",
      text: "The next step is creating Characters. These are the heroes, villains, and sidekicks that players will interact with and control.",
    },
    {
      type: "textMessage",
      text: "Like casting for a movie, choosing the right characters brings the game to life and adds depth to the story.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 4 },
        { direction: "up", steps: 3 },
        { direction: "right", steps: 6 },
        { direction: "left", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
  mapArray: [
    {
      type: "textMessage",
      text: "The Map is where the game takes place, and developers map out the world that players will explore.",
    },
    {
      type: "textMessage",
      text: "It's like drawing a treasure map, leading players to hidden secrets and epic journeys. Let's look at this level from above.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "right", steps: 7 },
        { direction: "up", steps: 3 },
        { direction: "right", steps: 6 },
        { direction: "down", steps: 3 },
        { direction: "up", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
  StoryArray: [
    {
      type: "textMessage",
      who: "npc",
      text: "But what holds it all together is the Story. This is the narrative thread that guides players through the game, giving them purpose and motivation.",
    },
    {
      type: "textMessage",
      text: "A captivating story can turn a simple game into an unforgettable experience, keeping players hooked from start to finish.",
    },
    {
      type: "walking",
      who: "npc",
      walks: [
        { direction: "down", steps: 3 },
        { direction: "right", steps: 14 },
        { direction: "up", steps: 1 },
        { direction: "down", steps: 0 },
      ],
      stopper: false,
      count: 1,
    },
  ],
};

let room_1_gd = {
  lowerSrc: "./assets/rooms/1_room/room_1_bg.png",
  gameObjects: {
    player: new Person({
      position: utils.gridToPos({ x: 2, y: 3 }),
      isPlayerControlled: true,
      src: "./assets/player/player_basic.png",
      useAnim: false,
      dontUseHalfY: true,
      size: { width: 11, height: 11 },
      offset: { x: 10, y: 16 },
    }),
    npc: new Person({
      position: utils.gridToPos({ x: 17, y: 8 }),
      src: "./assets/npc/npc_basic.png",
      useShadow: false,
      useAnim: false,
      dontUseHalfY: true,
      speed: 1,
      size: { width: 12, height: 14 },
      offset: { x: 10, y: 14 },
      behaviorLoop: [],

      talking: [],
    }),
  },

  walls: walls_room_1_gd,

  useMusic: false,
  cutsceneSpaces: {
    "3,2": infoTexts_r1.mechArray,
    "3,3": infoTexts_r1.mechArray,

    "14,8": infoTexts_r1.charArray,
    "14,9": infoTexts_r1.charArray,

    "25,4": infoTexts_r1.mapArray,
    "25,5": infoTexts_r1.mapArray,

    "39,3": infoTexts_r1.StoryArray,
    "40,3": infoTexts_r1.StoryArray,
  },

  nextRooms: {
    "53,6": "hall_1",
  },

  restartPositions: [{ x: 2, y: 3 }],
};
