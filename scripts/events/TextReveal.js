const maxSpeedTR = 101;

/**
 * TextReveal class to gradually reveal text character by character.
 * Allows configuring the text element, text content, and speed of the reveal.
 */
class TextReveal {
  constructor(config) {
    this.element = config.element; // The HTML element where the text will be revealed
    this.text = config.text; // The text content to be revealed
    this.speed = config.speed || 50; // Speed of the reveal in milliseconds per character

    this.timeout = null; // Timeout reference for controlling the reveal
    this.isDone = false; // Flag to check if the reveal is completed
  }

  /**
   * Reveal one character from the list.
   * @param {Array} list - List of character objects to reveal.
   */
  revealOneCharacter(list) {
    const next = list.splice(0, 1)[0];
    next.span.classList.add("revealed"); // Add 'revealed' class to the next character

    if (list.length > 0) {
      // Continue revealing the next character after a delay
      this.timeout = setTimeout(() => {
        this.revealOneCharacter(list);
      }, maxSpeedTR - next.delayAfter);
    } else {
      this.isDone = true; // Mark the reveal as done when all characters are revealed
    }
  }

  /**
   * Immediately reveal all characters.
   */
  toDone() {
    clearTimeout(this.timeout); // Clear the ongoing timeout
    this.isDone = true; // Mark the reveal as done
    // Add 'revealed' class to all characters
    this.element.querySelectorAll("span").forEach((span) => {
      span.classList.add("revealed");
    });
  }

  /**
   * Initialize the text reveal process.
   * Split the text into characters and start the reveal.
   */
  init() {
    let characters = [];
    this.text.split("").forEach((character) => {
      let span = document.createElement("span");
      span.textContent = character;
      this.element.appendChild(span);

      characters.push({
        span,
        delayAfter: character === " " ? maxSpeedTR : this.speed, // Adjust delay for spaces
      });
    });

    this.revealOneCharacter(characters); // Start revealing characters
  }
}
