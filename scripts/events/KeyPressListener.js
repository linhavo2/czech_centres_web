/**
 * KeyPressListener class to handle key press events.
 * Allows binding and unbinding of key press listeners for specific keys with callbacks.
 */
class KeyPressListener {
  constructor(keyCode, callback) {
    let keySafe = true; // Flag to ensure the callback is called only once per key press

    // Function to handle keydown events
    this.keydownFunction = function (event) {
      if (event.code === keyCode) {
        if (keySafe) {
          keySafe = false;
          callback(); // Execute the callback function when the key is pressed
        }
      }
    };

    // Function to handle keyup events
    this.keyupFunction = function (event) {
      if (event.code === keyCode) {
        keySafe = true; // Reset the flag when the key is released
      }
    };

    // Add event listeners for keydown and keyup events
    document.addEventListener("keydown", this.keydownFunction);
    document.addEventListener("keyup", this.keyupFunction);
  }

  /**
   * Unbind the key press event listeners.
   */
  unbind() {
    document.removeEventListener("keydown", this.keydownFunction);
    document.removeEventListener("keyup", this.keyupFunction);
  }
}
