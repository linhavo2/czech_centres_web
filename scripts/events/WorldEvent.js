/**
 * WorldEvent class to handle various game events such as idle, walk, teleport, etc.
 * The events are triggered based on the provided configuration and map state.
 */
class WorldEvent {
  constructor({ map, event }) {
    this.map = map; // The current game map
    this.event = event; // The event configuration
  }

  /**
   * Handle idle event.
   * @param {function} resolve - The resolve function to complete the event.
   */
  idle(resolve) {
    const who = this.map.gameObjects[this.event.who];

    // Start the idle behavior
    who.startBehavior(
      { map: this.map },
      {
        type: "idle",
        direction: this.event.direction,
        duration: this.event.duration,
      }
    );

    // Event handler for idle completion
    const completeHandler = (e) => {
      if (e.detail.whoId === this.event.who) {
        document.removeEventListener("BehaviorIdleComplete", completeHandler);
        resolve();
      }
    };

    document.addEventListener("BehaviorIdleComplete", completeHandler);
  }

  /**
   * Handle walk event.
   * @param {function} resolve - The resolve function to complete the event.
   */
  walk(resolve) {
    const who = this.map.gameObjects[this.event.who];

    // Start the walk behavior
    who.startBehavior(
      { map: this.map },
      { type: "walk", direction: this.event.direction, steps: this.event.steps }
    );

    // Event handler for walk completion
    const completeHandler = (e) => {
      if (e.detail.whoId === this.event.who) {
        document.removeEventListener("BehaviorWalkComplete", completeHandler);
        resolve();
      }
    };

    document.addEventListener("BehaviorWalkComplete", completeHandler);
  }

  /**
   * Handle multiple walking events in sequence.
   * @param {function} resolve - The resolve function to complete the event.
   * @returns {Promise} - A promise chain for sequential walking.
   */
  walking(resolve) {
    function startWalk(map, whoId, direction, steps) {
      return new Promise((resolve) => {
        const who = map.gameObjects[whoId];

        // Event handler to resolve when the walk is complete
        const completeHandler = (e) => {
          if (e.detail.whoId === whoId) {
            document.removeEventListener(
              "BehaviorWalkComplete",
              completeHandler
            );
            resolve();
          }
        };

        document.addEventListener("BehaviorWalkComplete", completeHandler);

        who.startBehavior({ map }, { type: "walk", direction, steps });
      });
    }

    let promiseChain = Promise.resolve(); // Initialize the Promise chain

    // Chain promises for each walk
    this.event.walks.forEach((walk) => {
      promiseChain = promiseChain.then(() =>
        startWalk(this.map, this.event.who, walk.direction, walk.steps)
      );
    });

    return promiseChain; // Return the chained Promises
  }

  /**
   * Handle teleport event.
   * @param {function} resolve - The resolve function to complete the event.
   */
  teleport(resolve) {
    const who = this.map.gameObjects[this.event.who];
    who.position = this.event.position;
    who.direction = this.event.direction;
    resolve();
  }

  /**
   * Handle spawn event.
   * @param {function} resolve - The resolve function to complete the event.
   */
  spawn(resolve) {
    const who = this.map.gameObjects[this.event.who];
    who.position = this.event.position;
    who.direction = this.event.direction;
    who.visible = true;
    resolve();
  }

  /**
   * Handle despawn event.
   * @param {function} resolve - The resolve function to complete the event.
   */
  despawn(resolve) {
    const who = this.map.gameObjects[this.event.who];
    who.visible = false;
    who.position = { x: 0, y: 0 };
    resolve();
  }

  /**
   * Handle text message event.
   * @param {function} resolve - The resolve function to complete the event.
   */
  textMessage(resolve) {
    if (this.event.faceHero) {
      const obj = this.map.gameObjects[this.event.faceHero];
      var previousDirection = obj.direction;
      obj.direction = utils.oppositeDirection(
        this.map.gameObjects["player"].direction
      );
    }

    const textMessage = new TextMessage({
      text: utils.getTranslation(this.event.text), // The text message
      onComplete: () => resolve(), // Resolve the promise when the text message is done
    });

    textMessage.init(document.querySelector(".game-container"));
  }

  /**
   * Handle switch sprite event.
   * @param {function} resolve - The resolve function to complete the event.
   */
  switchSprite(resolve) {
    switch (this.event.what) {
      case "map":
        this.map.switchSprite(this.event.where, this.event.src);
        break;
      default:
        this.map.gameObjects[this.event.what].switchSprite(this.event.sprite);
    }

    resolve();
  }

  /**
   * Handle use sound event.
   * @param {function} resolve - The resolve function to complete the event.
   */
  useSound(resolve) {
    this.map.gameObjects[this.event.who].steps.useSound = true;
    resolve();
  }

  /**
   * Handle use music event.
   * @param {function} resolve - The resolve function to complete the event.
   */
  useMusic(resolve) {
    this.map.useMusic = true;
    this.map.initMusic();
    resolve();
  }

  /**
   * Handle voice event.
   * @param {function} resolve - The resolve function to complete the event.
   */
  voice(resolve) {
    let voice = new Audio(this.event.src);
    voice.volume = 0.5;
    console.log("Playing voice");
    voice.play();
    resolve();
  }

  /**
   * Handle pause event.
   * @param {function} resolve - The resolve function to complete the event.
   */
  pause(resolve) {
    this.map.isPaused = true;
    const menu = new PauseMenu({
      onComplete: () => {
        this.map.isPaused = false;
        resolve();
      },
    });

    menu.init(document.querySelector(".game-container"));
  }

  /**
   * Handle restart index event.
   * @param {function} resolve - The resolve function to complete the event.
   */
  restartIndex(resolve) {
    this.map.restart.index = Math.min(
      this.map.restart.index + 1,
      this.map.restart.positions.length - 1
    );
    resolve();
  }

  /**
   * Initialize the event based on the event type and handle event count.
   * @returns {Promise} - A promise that resolves when the event is complete.
   */
  init() {
    if (this.event.count === 0) {
      return Promise.resolve();
    } else if (this.event.count > 0) {
      this.event.count--;
    }

    return new Promise((resolve) => {
      this[this.event.type](resolve);
    });
  }
}
