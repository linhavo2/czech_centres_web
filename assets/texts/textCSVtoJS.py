import csv
import json

def csv_to_js():
    csv_filepath = 'texts.csv'  # Updated to the correct filename
    js_filepath = '../../scripts/utils/translations.js'
    
    try:
        with open(csv_filepath, newline='', encoding='utf-8-sig') as csvfile:
            reader = csv.DictReader(csvfile, delimiter='\t')  # Use tab as the delimiter
            headers = reader.fieldnames
            if "Default" not in headers:
                raise ValueError("CSV does not contain a 'Default' column. Found headers: " + str(headers))
            
            data = {}
            for row in reader:
                key = row['Default']
                data[key] = {language: row[language] for language in headers if language != 'Default'}
    
        with open(js_filepath, 'w', encoding='utf-8') as jsfile:
            jsfile.write('const translations = ')
            json.dump(data, jsfile, indent=4, ensure_ascii=False)  # Avoid escaping Unicode characters
            jsfile.write(';')
            print("Successfully converted CSV to JS. Output file:", js_filepath)
    except Exception as e:
        print("An error occurred:", e)

if __name__ == "__main__":
    csv_to_js()
