import json
import argparse
import os

def read_grid_from_file(filename):
    grid = []
    with open(filename, 'r') as file:
        for line in file:
            grid.append([int(cell) for cell in line.strip().split('\t')])
    return grid

def is_neighbor_zero(x, y, grid):
    # Check neighboring cells for zeros
    max_y = len(grid)
    max_x = len(grid[0]) if max_y > 0 else 0
    directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]  # Right, Down, Left, Up
    for dx, dy in directions:
        nx, ny = x + dx, y + dy
        if 0 <= nx < max_x and 0 <= ny < max_y:
            if grid[ny][nx] == 0:
                return True
    return False

def export_grid_to_json(grid, json_filename, variable_name):
    walls = {}
    for y, row in enumerate(grid):
        for x, cell in enumerate(row):
            if cell == 1 and is_neighbor_zero(x, y, grid):
                # Convert tuple (x, y) to string "x,y"
                walls[f"{x},{y}"] = True

    # Format the variable definition for JavaScript
    with open("../../scripts/world/rooms/walls/"+json_filename, 'w') as file:
        json_content = json.dumps(walls)
        file.write(f'const {variable_name} = {json_content};')

def remove_dot_slash(filename):
    # Remove '.\' from the beginning of the filename
    if filename.startswith('.\\'):
        filename = filename[2:]  # Remove the first two characters
    return filename

def main():
    parser = argparse.ArgumentParser(description="Convert a grid file to a JavaScript JSON representation of walls.")
    parser.add_argument("grid_file", help="Path to the grid file.")
    args = parser.parse_args()

    args.grid_file = remove_dot_slash(args.grid_file)

    grid = read_grid_from_file(args.grid_file)
    # Extract the base name without the extension and add .js extension
    base_name = os.path.splitext(args.grid_file)[0]
    json_file = f'walls_{base_name}.js'  # Change to .js to reflect JavaScript content

    # Create a JavaScript variable name dynamically based on the file name
    variable_name = f'walls_{os.path.basename(base_name)}'

    export_grid_to_json(grid, json_file, variable_name)
    print(f'JavaScript JSON file has been generated: {json_file}')

if __name__ == "__main__":
    main()
